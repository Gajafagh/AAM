import 'package:equatable/equatable.dart';
import 'package:GajAfagh/domain/model/ticket_user.dart';

class TicketUserDto extends Equatable {
  final String id;
  final String title;
  final String desc;
  final String date;
  final String type;
  final String status;
  final String userId;
  final String sender;
  final List withFile;
  final String seen;
  final String closed;
  final String ticketnumber;

  TicketUserDto(
      {required this.id,
      required this.title,
      required this.desc,
      required this.date,
      required this.type,
      required this.status,
      required this.userId,
      required this.sender,
      required this.withFile,
      required this.seen,
      required this.closed,
      required this.ticketnumber});

  TicketUserDto.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        desc = json['description'],
        status = json['status'],
        type = json['requestType'],
        date = json['date'],
        id = json['_id'],
        userId = json['user'],
        sender = json['sender'],
        withFile = json['withFile'],
        seen = json['seen'],
        closed = json['closed'],
        ticketnumber = json['ticketNumber'];

  TicketUserModel toTicketUserModel() {
    return TicketUserModel(
        id: id,
        title: title,
        desc: desc,
        date: date,
        type: type,
        status: status,
        sender: sender,
        withFile: withFile,
        seen: seen,
        closed: closed,
        ticketNumber: ticketnumber);
  }

  @override
  List<Object> get props =>
      [id, title, desc, date, type, status, userId, sender, seen, closed];
}

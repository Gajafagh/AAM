import 'package:equatable/equatable.dart';

class TicketRequest extends Equatable {
  final String title;
  final String desc;
  final String type;
  final String withFile;

  const TicketRequest({
    required this.title,
    required this.desc,
    required this.type,
    required this.withFile,
  });

  @override
  List<Object> get props => [title, desc, type, withFile];

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'description': desc,
      'requestType': type,
      'withFile': withFile,
    };
  }
}

class AnswerRequest extends Equatable {
  final String title;
  final String desc;

  const AnswerRequest({
    required this.title,
    required this.desc,
  });

  @override
  List<Object> get props => [title, desc];

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'description': desc,
      'requestType': 'forwarded',
    };
  }
}

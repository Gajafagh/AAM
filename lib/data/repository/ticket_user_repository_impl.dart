import 'package:GajAfagh/data/remote/api_service.dart';
import 'package:GajAfagh/data/remote/dto/ticket_request.dart';
import 'package:GajAfagh/data/remote/dto/ticket_user_response.dart';
import 'package:GajAfagh/data/remote/dto/user_info_response.dart';
import 'package:GajAfagh/domain/repository/ticket_user_repository.dart';

class TicketUserRepositoryImpl implements TicketUserRepository {
  final ApiService apiService;

  TicketUserRepositoryImpl(this.apiService);

  @override
  Future<bool> createTicket(TicketRequest ticketRequest) {
    return apiService.createTicket(ticketRequest);
  }

  @override
  Future<List<TicketUserDto>> getAllUserTicket() {
    return apiService.getAllUserTicket();
  }

  @override
  Future<List<TicketUserDto>> getAllTicketUserFiltered(String status) {
    return apiService.getAllTicketUserFiltered(status);
  }

  @override
  Future<bool> deleteTicket(String ticketId) {
    return apiService.deleteTicket(ticketId);
  }

  @override
  Future<bool> forwardTicket(String ticketId) {
    return apiService.forwardTicket(ticketId);
  }

  @override
  Future<bool> uploadTicket(List<int> bytes, String name) {
    return apiService.uploadTicket(bytes, name);
  }

  @override
  Future<bool> replyTicket(String ticketId) {
    return apiService.replyTicket(ticketId);
  }

  @override
  Future<bool> userToken(String token) {
    return apiService.userToken(token);
  }

  @override
  Future<UserInfoResponse> userInfo(String userId) {
    return apiService.userInfo(userId);
  }
}

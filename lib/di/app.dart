import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:GajAfagh/data/local/app_preferences.dart';
import 'package:GajAfagh/data/remote/api_service.dart';
import 'package:GajAfagh/data/remote/dio_api_service.dart';
import 'package:GajAfagh/data/repository/auth_repository_impl.dart';
import 'package:GajAfagh/data/repository/ticket_user_repository_impl.dart';
import 'package:GajAfagh/domain/repository/auth_repository.dart';
import 'package:GajAfagh/domain/repository/ticket_user_repository.dart';
import 'package:GajAfagh/data/remote/util/constant.dart';

final instance = GetIt.instance;

Future<void> initModule() async {
  //dio instance
  final dio = Dio(
    BaseOptions(
      baseUrl: RemoteConstant.baseUrl,
    ),
  );
  instance.registerLazySingleton<Dio>(() => dio);

  //api service
  final DioApiService apiService = DioApiService(instance<Dio>());
  instance.registerLazySingleton<ApiService>(() => apiService);

  //shared preferences
  final SharedPreferences sharedPreferences =
      await SharedPreferences.getInstance();
  instance.registerLazySingleton<SharedPreferences>(
    () => sharedPreferences,
  );
  //shared preferences implemented
  instance.registerLazySingleton<AppPreferences>(
      () => AppPreferences(instance<SharedPreferences>()));

  //repositories
  final AuthRepository authRepository = AuthRepositoryImpl(
    instance<ApiService>(),
    instance<AppPreferences>(),
  );
  await authRepository.loadTokenFromDb();
  instance.registerLazySingleton(() => authRepository);

  final TicketUserRepository ticketUserRepository = TicketUserRepositoryImpl(
    instance<ApiService>(),
  );
  instance.registerLazySingleton(() => ticketUserRepository);
}

import 'package:GajAfagh/data/remote/dto/ticket_request.dart';
import 'package:GajAfagh/data/remote/dto/ticket_user_response.dart';
import 'package:GajAfagh/data/remote/dto/user_info_response.dart';

abstract class TicketUserRepository {
  Future<bool> createTicket(TicketRequest ticketRequest);

  Future<List<TicketUserDto>> getAllUserTicket();

  Future<List<TicketUserDto>> getAllTicketUserFiltered(String status);

  Future<bool> deleteTicket(String ticketId);

  Future<bool> forwardTicket(String ticketId);

  Future<bool> replyTicket(String ticketId);

  Future<void> userToken(String token);

  Future<bool> uploadTicket(List<int> bytes, String name);

  Future<UserInfoResponse> userInfo(String userId);
}

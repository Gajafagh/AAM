import 'package:equatable/equatable.dart';

class TicketUserModel extends Equatable {
  final String id;
  final String title;
  final String desc;
  final String date;
  final String type;
  final String status;
  final String sender;
  final List withFile;
  final String seen;
  final String closed;
  final String ticketNumber;

  const TicketUserModel(
      {required this.id,
      required this.title,
      required this.desc,
      required this.date,
      required this.type,
      required this.status,
      required this.sender,
      required this.withFile,
      required this.seen,
      required this.closed,
      required this.ticketNumber});

  @override
  List<Object> get props => [id, title, desc, date, type, status, sender, seen];
}

import 'package:GajAfagh/presentation/screen/splash.dart';
import 'package:GajAfagh/util/extension.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:GajAfagh/data/local/app_preferences.dart';
import 'package:GajAfagh/di/app.dart';
import 'package:GajAfagh/domain/model/token_container.dart';
import 'package:GajAfagh/presentation/route.dart';
import 'package:GajAfagh/presentation/theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await initModule();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final AppRoute _appRoute = AppRoute();
  final GetIt instance = GetIt.instance;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    AwesomeNotifications().initialize(
        // set the icon to null if you want to use the default app icon
        null,
        [
          NotificationChannel(
              channelGroupKey: 'basic_channel_group',
              channelKey: 'basic_channel',
              channelName: 'Basic notifications',
              channelDescription: 'Notification channel for basic tests',
              defaultColor: Color(0xFF9D50DD),
              ledColor: Colors.white)
        ],
        // Channel groups are only visual and are not required
        channelGroups: [
          NotificationChannelGroup(
              channelGroupkey: 'basic_channel_group',
              channelGroupName: 'Basic group')
        ],
        debug: true);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'GajAfagh',
      theme: AppThemeConfig.light().getTheme(),
      onGenerateRoute: _appRoute.onGenerateRoute,
    );
  }

  @override
  void dispose() async {
    _appRoute.dispose();

    //save token in sharedPreferences
    if (TokenContainer.instance().accessToken != null &&
        TokenContainer.instance().refreshToken != null) {
      await instance<AppPreferences>().saveAllToken(
        TokenContainer.instance().accessToken!,
        TokenContainer.instance().refreshToken!,
      );
    }
    super.dispose();
  }
}

import 'package:flutter/material.dart';
import 'package:GajAfagh/presentation/component/dimension.dart';

class CustomMenuItem extends StatelessWidget {
  final Widget icon;
  final Text text;
  final Color color;
  final Function() onTap;

  const CustomMenuItem(
      {Key? key,
      required this.icon,
      required this.text,
      required this.onTap,
      required this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    return Material(
      color: Colors.transparent,
      child: InkWell(
        borderRadius: circular28,
        onTap: onTap,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 22, vertical: 20),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: color,
                blurRadius: 30,
              )
            ],
            borderRadius: circular28,
            color: color,
          ),
          child: Row(
            children: [
              icon,
              sizedBoxW12,
              text,
            ],
          ),
        ),
      ),
    );
  }
}

import 'dart:ui';

import 'package:GajAfagh/presentation/color.dart';
import 'package:GajAfagh/presentation/component/widget/gradiant_text.dart';
import 'package:GajAfagh/presentation/screen/home/home_route.dart';
import 'package:GajAfagh/presentation/screen/settings/setting.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_home.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:GajAfagh/domain/repository/auth_repository.dart';
import 'package:GajAfagh/domain/repository/ticket_user_repository.dart';
import 'package:GajAfagh/gen/assets.gen.dart';
import 'package:GajAfagh/presentation/component/dimension.dart';
import 'package:GajAfagh/presentation/component/widget/small_widget.dart';
import 'package:GajAfagh/presentation/screen/auth/login_screen.dart';
import 'package:GajAfagh/presentation/screen/home/appbar.dart';
import 'package:GajAfagh/presentation/screen/home/avatar.dart';
import 'package:GajAfagh/presentation/screen/home/exit_dialog.dart';
import 'package:GajAfagh/presentation/screen/home/menu_item.dart';
import 'package:GajAfagh/presentation/screen/ticket/add_ticket/add_ticket_screen.dart';
import 'package:GajAfagh/presentation/screen/ticket/list_ticket/list_ticket_screen.dart';
import 'bloc/home_bloc.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:GajAfagh/presentation/screen/ticket/add_ticket/drop_down.dart';

_launchCaller() async {
  const url = "tel:02156328245";
  launchUrlString(url);
}

class PushNotification {
  PushNotification({
    this.title,
    this.body,
  });

  String? title;
  String? body;
}

class HomeScreen extends StatefulWidget {
  static const String route = '/home';
  static const String item1 = 'تیکت های شما';
  static const String item2 = 'ارسال تیکت جدید';
  static const String item3 = 'تنظیمات';
  static const String item6 = 'پشتیبانی و راهنما';
  static const String item4 = 'خروج';
  static const String item5 = 'برنامه ریزی';
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final getIt = GetIt.instance;
  HomeBloc? _bloc;
  var _totalNotifications = 0;
  var _notificationInfo;

  @override
  void initState() {
    // checkForInitialMessage();
    deparLists();
    super.initState();
  }

  @override
  void dispose() {
    _bloc?.close();
    super.dispose();
  }

  // Future<String> getToken() async {
  //   final messaging = FirebaseMessaging.instance;
  //   final fcmToken = await messaging.getToken();
  //   return fcmToken!;
  // }

  // checkForInitialMessage() async {
  //   final messaging = FirebaseMessaging.instance;

  //   final settings = await messaging.requestPermission(
  //     alert: true,
  //     announcement: false,
  //     badge: true,
  //     carPlay: false,
  //     criticalAlert: false,
  //     provisional: false,
  //     sound: true,
  //   );

  //   FirebaseMessaging.onMessageOpenedApp
  //       .listen((event) => _firebaseMessagingBackgroundHandler(event));

  //   final fcmToken = await FirebaseMessaging.instance.getToken();
  //   FirebaseMessaging.onMessage.listen((RemoteMessage? message) =>
  //       _firebaseMessagingBackgroundHandler(message!));

  //   FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  // }

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    final Size size = MediaQuery.of(context).size;

    //change status bar color
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: themeData.colorScheme.surfaceVariant,
      statusBarIconBrightness: Brightness.dark,
      systemNavigationBarIconBrightness: Brightness.dark,
      systemNavigationBarColor: themeData.colorScheme.surfaceVariant,
    ));

    //handle only portrait Screen
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return Scaffold(
      backgroundColor: themeData.colorScheme.surfaceVariant,
      body: BlocProvider<HomeBloc>(
        create: (context) {
          _bloc = HomeBloc(
            getIt<TicketUserRepository>(),
            getIt<AuthRepository>(),
          );

          _bloc!.add(HomeStarted(""));

          return _bloc!;
        },
        child: SafeArea(
          child: SingleChildScrollView(
            child: BlocBuilder<HomeBloc, HomeState>(
              buildWhen: (p, c) => c is HomeSuccess || c is HomeLoading,
              builder: (context, state) {
                return state is HomeLoading
                    ?
                    //show loading
                    _loading(size, themeData)
                    :
                    //content

                    Padding(
                        padding: padding36H,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            sizedBoxH24,

                            //appbar

                            const HomeAppBar(),

                            sizedBoxH24,

                            //appbar
                            const AvatarWidget(),

                            sizedBoxH12,

                            //name text
                            GradientText('به آفاق خوش آمدید',
                                style: themeData.textTheme.headline3!.copyWith(
                                  fontWeight: FontWeight.w700,
                                ),
                                gradient:
                                    LightColorPalette.defaultTextGradiant),

                            sizedBoxH36,

                            //item list ticket
                            CustomMenuItem(
                              color: Color.fromARGB(110, 77, 77, 77),
                              icon: Assets.image.svg.myTicket.svg(),
                              text: Text(HomeScreen.item1,
                                  style:
                                      themeData.textTheme.headline5!.copyWith(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    fontWeight: FontWeight.w700,
                                  )),
                              onTap: () {
                                Navigator.pushNamed(
                                    context, ListTicketScreen.route);
                              },
                            ),

                            sizedBoxH20,

                            //item create ticket
                            CustomMenuItem(
                              color: Color.fromARGB(110, 77, 77, 77),
                              icon: Assets.image.svg.allTicket.svg(),
                              text: Text(HomeScreen.item2,
                                  style:
                                      themeData.textTheme.headline5!.copyWith(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    fontWeight: FontWeight.w700,
                                  )),
                              onTap: () {
                                Navigator.pushNamed(
                                    context, AddNewTicketScreen.route);
                              },
                            ),

                            sizedBoxH20,

                            //item change password
                            CustomMenuItem(
                              color: Color.fromARGB(110, 77, 77, 77),
                              icon: Assets.image.svg.passwordItem.svg(),
                              text: Text(
                                HomeScreen.item3,
                                style: themeData.textTheme.headline5!.copyWith(
                                  color: Color.fromARGB(255, 255, 255, 255),
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              onTap: () {
                                Navigator.pushNamed(
                                    context, SettingScreen.route);
                              },
                            ),

                            sizedBoxH20,

                            CustomMenuItem(
                              color: Color.fromARGB(110, 77, 77, 77),
                              icon: Assets.image.svg.person.svg(),
                              text: Text(HomeScreen.item5,
                                  style:
                                      themeData.textTheme.headline5!.copyWith(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    fontWeight: FontWeight.w700,
                                  )),
                              onTap: () {
                                Navigator.pushNamed(context, TodoScreen.route);
                              },
                            ),

                            sizedBoxH20,

                            //item exit
                            CustomMenuItem(
                              color: Color.fromARGB(167, 39, 133, 47),
                              icon: Assets.image.svg.search.svg(),
                              text: Text(HomeScreen.item6,
                                  style:
                                      themeData.textTheme.headline5!.copyWith(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    fontWeight: FontWeight.w700,
                                  )),
                              onTap: () {
                                _launchCaller();
                              },
                            ),

                            sizedBoxH20,
                            //item exit
                            CustomMenuItem(
                              color: Color.fromARGB(103, 255, 105, 105),
                              icon: Assets.image.svg.exit.svg(),
                              text: Text(HomeScreen.item4,
                                  style:
                                      themeData.textTheme.headline5!.copyWith(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    fontWeight: FontWeight.w700,
                                  )),
                              onTap: () async {
                                await showCustomDialog(
                                  context: context,
                                  title: 'آیا از خروج خود اطمینان دارید؟',
                                );
                              },
                            ),
                            const SizedBox(
                              height: HomeRoute.bottomNavHeight -
                                  HomeRoute.bottomNavContainerHeight,
                            ),
                          ],
                        ),
                      );
              },
            ),
          ),
        ),
      ),
    );
  }

  Text _menuItemText(ThemeData themeData, String text) {
    return Text(
      text,
      style: themeData.textTheme.subtitle1,
    );
  }

  Widget _nameText(ThemeData themeData, String text) {
    return Text(
      text,
      style: themeData.textTheme.headline3,
    );
  }

  Widget _loading(Size size, ThemeData themeData) {
    return SizedBox(
      width: size.width,
      height: size.height,
      child: Center(
        child: showLoading(
          themeData.colorScheme.primary,
        ),
      ),
    );
  }

  showCustomDialog({
    required BuildContext context,
    required String title,
  }) async {
    await showDialog(
      context: context,
      barrierColor: null,
      builder: (context) {
        return BlocProvider.value(
          value: _bloc!,
          child: BlocListener<HomeBloc, HomeState>(
            listenWhen: (p, c) {
              return c is HomeExitSuccess;
            },
            listener: (context, state) {
              if (state is HomeExitSuccess) {
                _backToHome(context);
              }
            },
            //for rtl layout
            child: Directionality(
              textDirection: TextDirection.rtl,
              child: BackdropFilter(
                //for blur behind screen
                filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                child: ExitDialog(
                  title: title,
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  void _backToHome(BuildContext context) {
    Navigator.pushNamedAndRemoveUntil(
        context, LoginScreen.route, (route) => false);
  }
}

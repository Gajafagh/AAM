import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:GajAfagh/gen/assets.gen.dart';
import 'package:GajAfagh/presentation/color.dart';
import 'package:GajAfagh/presentation/component/dimension.dart';
import 'package:GajAfagh/presentation/component/widget/gradiant_text.dart';
import 'package:persian_datetime_picker/persian_datetime_picker.dart';

Future Cland(BuildContext context) async {
  Jalali? pickedDate = await showModalBottomSheet<Jalali>(
    context: context,
    builder: (context) {
      Jalali? tempPickedDate;
      return Container(
        height: 250,
        child: Column(
          children: <Widget>[
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  CupertinoButton(
                    child: Text(
                      'لغو',
                      style: TextStyle(
                        fontFamily: 'Dana',
                      ),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  CupertinoButton(
                    child: Text(
                      'تایید',
                      style: TextStyle(
                        fontFamily: 'Dana',
                      ),
                    ),
                    onPressed: () {
                      print(tempPickedDate);

                      Navigator.of(context).pop(tempPickedDate);
                    },
                  ),
                ],
              ),
            ),
            Divider(
              height: 0,
              thickness: 1,
            ),
            Expanded(
              child: Container(
                child: CupertinoTheme(
                  data: CupertinoThemeData(
                    textTheme: CupertinoTextThemeData(
                      dateTimePickerTextStyle: TextStyle(fontFamily: 'Dana'),
                    ),
                  ),
                  child: PCupertinoDatePicker(
                    mode: PCupertinoDatePickerMode.time,
                    onDateTimeChanged: (Jalali dateTime) {
                      tempPickedDate = dateTime;
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    },
  );
}

class HomeAppBar extends StatelessWidget {
  const HomeAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);
    return Row(
      children: [
        GradientText(
          'پلتفرم اتوماسیون اداری',
          gradient: LightColorPalette.defaultTextGradiant,
          style: themeData.textTheme.headline5,
        ),
        //logo
        Image.asset(
          "assets/image/png/icon.png",
          scale: 6,
        ),

        sizedBoxW12,

        //title

        const Spacer(),
      ],
    );
  }
}

class Clands extends StatelessWidget {
  const Clands({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);
    Cland(context);
    return Spacer();
  }
}

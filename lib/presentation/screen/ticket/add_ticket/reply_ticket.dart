import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_it/get_it.dart';
import 'package:GajAfagh/domain/repository/ticket_user_repository.dart';
import 'package:GajAfagh/gen/assets.gen.dart';
import 'package:GajAfagh/presentation/color.dart';
import 'package:GajAfagh/presentation/component/dimension.dart';
import 'package:GajAfagh/presentation/component/widget/elevated_textfield.dart';
import 'package:GajAfagh/presentation/component/widget/gradiant_button.dart';
import 'package:GajAfagh/presentation/component/widget/small_widget.dart';
import 'package:GajAfagh/presentation/screen/home/appbar.dart';
import 'package:GajAfagh/presentation/screen/home/home_route.dart';
import 'package:GajAfagh/presentation/screen/ticket/add_ticket/bloc/add_ticket_bloc.dart';
import 'package:GajAfagh/presentation/screen/ticket/add_ticket/file.dart';
import 'package:GajAfagh/util/extension.dart';

import 'drop_down.dart';

class ReplyTicketScreen extends StatefulWidget {
  static const String route = 'reply-ticket';

  const ReplyTicketScreen({Key? key}) : super(key: key);

  @override
  State<ReplyTicketScreen> createState() => _ReplyTicketScreenState();
}

class _ReplyTicketScreenState extends State<ReplyTicketScreen> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descController = TextEditingController();
  AddTicketBloc? _bloc;
  final GetIt instance = GetIt.instance;

  String _type = '';

  @override
  void dispose() {
    _titleController.dispose();
    _descController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);

    return HomeRoute(
      child: BlocProvider<AddTicketBloc>(
        create: (context) {
          _bloc = AddTicketBloc(instance<TicketUserRepository>());
          _bloc?.add(AddTicketStarted());
          return _bloc!;
        },
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Padding(
            padding: padding36H,
            child: BlocListener<AddTicketBloc, AddTicketState>(
              listener: (context, state) async {
                if (state is AddTicketError) {
                  _addTicketErrorResponse(context, state);
                } else if (state is AddTicketCreated) {
                  _addTicketCreatedSuccessResponse(context);
                } else if (state is AddTicketSetNewType) {
                  setNewType(state.value);
                }
              },
              listenWhen: (p, c) {
                return c is AddTicketError ||
                    c is AddTicketCreated ||
                    c is AddTicketSetNewType;
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  sizedBoxH24,
                  //appbar
                  const HomeAppBar(),

                  sizedBoxH48,

                  //title text
                  _largeText(themeData, 'پاسخ به تیکت :'),

                  sizedBoxH24,

                  //hint description text
                  _descriptionText(themeData, 'توضیحات :'),

                  sizedBoxH12,

                  //text field description request
                  ElevatedTextField(
                    keyboardType: TextInputType.multiline,
                    hint: 'توضیحات بیشتر...',
                    isPassword: false,
                    maxLines: 8,
                    height: 150,
                    controller: _descController,
                  ),

                  sizedBoxH48,

                  BlocBuilder<AddTicketBloc, AddTicketState>(
                    builder: (context, state) {
                      return state is AddTicketLoading
                          ?
                          //show loading for when send request to server
                          showLoading(themeData.colorScheme.primary)
                          :
                          //submit button
                          GradiantButton(
                              gradient: LightColorPalette.defaultOkButton,
                              onTap: () {
                                _createTicket(context);
                              },
                              label: 'ارسال',
                              icon: _sendIcon(),
                              textStyle: themeData.textTheme.button,
                              height: homeButtonSizeHeight,
                              width: homeButtonSizeWidth,
                              borderRadius: circular18,
                            );
                    },
                  ),

                  sizedBoxH32,

                  //for padding with bottomNav
                  const SizedBox(
                    height: HomeRoute.bottomNavHeight -
                        HomeRoute.bottomNavContainerHeight,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  SvgPicture _sendIcon() => Assets.image.svg.send.svg(width: 18);

  void _addTicketErrorResponse(BuildContext context, AddTicketError state) {
    context.showSnackBar(state.error.message);
  }

  void _addTicketCreatedSuccessResponse(BuildContext context) {
    Navigator.pop(context);
    context.showSnackBar('پیام مورد نظر با موفقیت ارسال شد');
  }

  Widget _largeText(ThemeData themeData, String text) {
    return Text(
      text,
      style: themeData.textTheme.headline2,
    );
  }

  Widget _descriptionText(ThemeData themeData, String text) {
    return Padding(
      padding: padding4R,
      child: Text(
        text,
        style: themeData.textTheme.subtitle2,
      ),
    );
  }

  void _createTicket(BuildContext context) {
    context.read<AddTicketBloc>().add(
          AddTicketSubmitButtonClicked(
              'REPLY', _descController.text, 'replyTo', 'false'),
        );
  }

  void setNewType(String type) {
    _type = type;
  }
}

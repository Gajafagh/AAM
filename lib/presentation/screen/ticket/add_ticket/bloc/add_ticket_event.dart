part of 'add_ticket_bloc.dart';

abstract class AddTicketEvent extends Equatable {
  const AddTicketEvent();

  @override
  List<Object> get props => [];
}

class AddTicketStarted extends AddTicketEvent {}

class AddTicketSubmitButtonClicked extends AddTicketEvent {
  final String title;
  final String desc;
  final String type;
  final String withFile;

  @override
  List<Object> get props => [title, desc, type, withFile];

  const AddTicketSubmitButtonClicked(
      this.title, this.desc, this.type, this.withFile);
}

class AnswerTicketSubmitButtonClicked extends AddTicketEvent {
  final String title;
  final String desc;

  @override
  List<Object> get props => [title, desc];

  const AnswerTicketSubmitButtonClicked(this.title, this.desc);
}

class AddTicketChangeTypeState extends AddTicketEvent {
  final String type;

  @override
  List<Object> get props => [type];

  const AddTicketChangeTypeState(this.type);
}

class AddTicketChangePercent extends AddTicketEvent {
  final String type;

  @override
  List<Object> get props => [type];

  const AddTicketChangePercent(this.type);
}

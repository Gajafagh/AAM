import 'dart:developer';
// ignore: depend_on_referenced_packages
import 'package:http_parser/http_parser.dart';
import 'package:get_it/get_it.dart';
import 'package:GajAfagh/data/remote/api_service.dart';
import 'package:GajAfagh/data/remote/dio_api_service.dart';
import 'package:dio/dio.dart';
import 'package:GajAfagh/data/repository/ticket_user_repository_impl.dart';
import 'package:GajAfagh/domain/repository/auth_repository.dart';
import 'package:GajAfagh/domain/repository/ticket_user_repository.dart';
import 'package:GajAfagh/data/remote/util/constant.dart';

final instance = GetIt.instance;
final DioApiService apiService = DioApiService(instance<Dio>());

class ProjectProvider {
  ProjectProvider();

  late Response _response;

  final _baseUrl = "http://185.156.172.47:5001/api/v1";

  test(List<int> bytes, String name) async {
    String extension = name.split(".").last;

    TicketUserRepositoryImpl(
      instance<ApiService>(),
    );
    var formData = FormData.fromMap({
      "file": MultipartFile.fromBytes(
        bytes,
        filename: name,
        contentType: MediaType("File", extension),
      ),
    });

    var response = await apiService.dio.post(RemoteConstant.uploadTicket,
        data: formData, onSendProgress: (received, total) {});
    //var ss = apiService.uploadTicket(bytes, name);
    //return ss;
    //_response = await _dio.post("$_baseUrl/user/upload-ticket", data: formData);
  }
}

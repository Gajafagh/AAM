import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:GajAfagh/presentation/color.dart';
import 'package:GajAfagh/presentation/component/dimension.dart';
import 'package:requests/requests.dart';

List<String>? items = ["انتخاب دپارتمان"];
List<String>? items2 = ["انتخاب شخص ..."];
Future deparLists() async {
  items = ["انتخاب دپارتمان"];
  var ss = await Requests.post(
      "https://appafagh.ir:5000/api/v1/user/departmans",
      body: {});
  var js = jsonDecode(ss.body);
  for (var i = 0; i < js.length; i++) {
    items!.add(js[i]["name"]);
  }
}

//Map of items and color in our dropdown menu
Map<String, Color> colorItems2 = {
  'تعیین شخص ...': LightColorPalette.blackTextColor,
  'مدیر عامل': LightColorPalette.greenDropDown,
  'جناب آقای تقی گل آرا': LightColorPalette.blackTextColor,
  'مدیریت رباط کریم': LightColorPalette.greenDropDown,
  'جناب آقای مهدی نوری': LightColorPalette.blackTextColor,
  'مدیریت نسیم شهر': LightColorPalette.greenDropDown,
  'جناب آقای محمد عسگری': LightColorPalette.blackTextColor,
  'مشاورین ارشد': LightColorPalette.greenDropDown,
  'جناب مهدی نوری': LightColorPalette.blackTextColor,
  'جناب محمد عسگری': LightColorPalette.blackTextColor,
  'جناب میلاد کلاته': LightColorPalette.blackTextColor,
  'مشاوره تحصیلی': LightColorPalette.greenDropDown,
  'سرکار خانم فاطمه بابایی': LightColorPalette.blackTextColor,
  'سرکار خانم سمانه محمد پور': LightColorPalette.blackTextColor,
  'سرکار خانم ساحل عزیزی': LightColorPalette.blackTextColor,
  'سرکار خانم شیوا فرهادی': LightColorPalette.blackTextColor,
  'سرکار خانم سمیرا نوری': LightColorPalette.blackTextColor,
  'سرکار خانم سمیه سالمی': LightColorPalette.blackTextColor,
  'واحد پذیریش': LightColorPalette.greenDropDown,
  'سرکار خانم الهام قره داغی': LightColorPalette.blackTextColor,
  'سرکار خانم فاطمه فراستی': LightColorPalette.blackTextColor,
  'سرکار خانم زهرا سلیمانی': LightColorPalette.blackTextColor,
  'سرکار خانم فاطمه طلعتی': LightColorPalette.blackTextColor,
  'حسابداری': LightColorPalette.greenDropDown,
  'جناب مسعود ابراهیمی': LightColorPalette.blackTextColor,
  'سرکار خانم مهسا مرادی': LightColorPalette.blackTextColor,
  'ناظر و مشاور مجموعه': LightColorPalette.greenDropDown,
  'جناب آقای شهرام گل آرا': LightColorPalette.blackTextColor,
  'واحد برنامه نویسی و رسانه': LightColorPalette.greenDropDown,
  'جناب آرش شهرابی ': LightColorPalette.blackTextColor,
  'جناب یونس صبری': LightColorPalette.blackTextColor,
  'جناب شاهین اظهری': LightColorPalette.blackTextColor,
  'جناب امیرحسین درویشی': LightColorPalette.blackTextColor,
  'جناب مهیار طاهری': LightColorPalette.blackTextColor,
};

class Dropdown extends StatefulWidget {
  final Function(String) onTap;

  const Dropdown({Key? key, required this.onTap}) : super(key: key);

  @override
  DropDownDeparType createState() => DropDownDeparType();
}

class DropDownDeparType extends State<Dropdown> {
  // List of items in our dropdown menu
  //Map of items and color in our dropdown menu
  static const Map<String, Color> colorItems = {
    'تعیین دپارتمان ...': LightColorPalette.blackTextColor,
    'مدیران': Color.fromARGB(255, 192, 110, 62),
    'مشاوران': Color.fromARGB(255, 128, 68, 143),
    'واحد پذیریش': Color.fromARGB(255, 128, 68, 143),
    'حسابداری': Color.fromARGB(255, 128, 68, 143),
    'ناظر و مشاور مجموعه': Color.fromARGB(255, 128, 68, 143),
    'سرپرست': Color.fromARGB(255, 128, 68, 143),
    'واحد برنامه نویسی و رسانه': Color.fromARGB(255, 128, 68, 143),
  };

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    return Container(
        decoration: BoxDecoration(
          color: themeData.colorScheme.surfaceVariant,
          border: _dropdownBorder(),
          boxShadow: [_dropDownShadow(themeData)],
          borderRadius: circular18,
        ),
        child: Column(children: <Widget>[
          _dropDownMenu(themeData),
          _dropDownMenu2(themeData),
        ]));
  }

  DropdownButtonFormField<String> _dropDownMenu2(ThemeData themeData) {
    return DropdownButtonFormField(
      focusColor: themeData.colorScheme.surfaceVariant,
      dropdownColor: themeData.colorScheme.surfaceVariant,
      borderRadius: circular18,
      isExpanded: true,
      isDense: true,
      icon: _dropDownIcon(),
      decoration: _inputDecoration2(themeData),
      items: items2!
          .map(
            (String item) => _dropDownMenuItem2(item, themeData),
          )
          .toList(),
      onChanged: (String? value) {
        widget.onTap(value!);
      },
    );
  }

  DropdownMenuItem<String> _dropDownMenuItem2(
      String item, ThemeData themeData) {
    return DropdownMenuItem(
      alignment: Alignment.centerRight,
      enabled: items2!.first == item ? false : true,
      value: item,
      child: items2!.first == item
          ? _disableItem(themeData, item)
          : _enableItem(item, themeData),
    );
  }

  DropdownButtonFormField<String> _dropDownMenu(ThemeData themeData) {
    return DropdownButtonFormField(
      focusColor: themeData.colorScheme.surfaceVariant,
      dropdownColor: themeData.colorScheme.surfaceVariant,
      borderRadius: circular18,
      isExpanded: true,
      isDense: true,
      icon: _dropDownIcon(),
      decoration: _inputDecoration(themeData),
      items: items!
          .map(
            (String item) => _dropDownMenuItem(item, themeData),
          )
          .toList(),
      onChanged: (String? value) {
        widget.onTap(value!);
      },
    );
  }

  DropdownMenuItem<String> _dropDownMenuItem(String item, ThemeData themeData) {
    return DropdownMenuItem(
      alignment: Alignment.centerRight,
      enabled: items!.first == item ? false : true,
      value: item,
      child: items!.first == item
          ? _disableItem(themeData, item)
          : _enableItem(item, themeData),
    );
  }

  InputDecoration _inputDecoration(ThemeData themeData) {
    return InputDecoration(
      filled: true,
      fillColor: themeData.colorScheme.surfaceVariant,
      focusedBorder: _dropdownButtonFormFieldBorder(),
      enabledBorder: _dropdownButtonFormFieldBorder(),
      isDense: true,
      contentPadding: _contentPadding(),
      hintText: items![0],
      hintStyle: themeData.textTheme.bodyText2!.apply(
        color: themeData.colorScheme.onSurface.withOpacity(0.7),
      ),
    );
  }

  InputDecoration _inputDecoration2(ThemeData themeData) {
    return InputDecoration(
      filled: true,
      fillColor: themeData.colorScheme.surfaceVariant,
      focusedBorder: _dropdownButtonFormFieldBorder(),
      enabledBorder: _dropdownButtonFormFieldBorder(),
      isDense: true,
      contentPadding: _contentPadding(),
      hintText: items2![0],
      hintStyle: themeData.textTheme.bodyText2!.apply(
        color: themeData.colorScheme.onSurface.withOpacity(0.7),
      ),
    );
  }

  Icon _dropDownIcon() => const Icon(Icons.arrow_drop_down);

  EdgeInsets _contentPadding() {
    return const EdgeInsets.only(
      top: 12,
      bottom: 12,
      right: 8,
      left: 16,
    );
  }

  OutlineInputBorder _dropdownButtonFormFieldBorder() {
    return const OutlineInputBorder(
      borderRadius: circular18,
      borderSide: BorderSide.none,
    );
  }

  Widget _disableItem(ThemeData themeData, String item) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Padding(
          padding: const EdgeInsets.only(
            right: 12,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Icon(
                Icons.arrow_drop_up,
                color: themeData.colorScheme.onSurface,
              ),
              Text(
                item,
                textDirection: TextDirection.rtl,
                style: themeData.textTheme.bodyText2!.apply(
                  color: themeData.colorScheme.onSurface.withOpacity(0.7),
                ),
              ),
            ],
          ),
        ),
        sizedBoxH12,
        const Divider(
          height: 1,
          color: Color(0xffD2D2D2),
        ),
      ],
    );
  }

  Widget _enableItem(String item, ThemeData themeData) {
    return Padding(
      padding: const EdgeInsets.only(right: 12),
      child: Text(
        item,
        style: themeData.textTheme.subtitle2!.copyWith(
          fontWeight: FontWeight.w600,
          color: Color.fromARGB(255, 128, 68, 143),
          fontSize: 16,
        ),
      ),
    );
  }

  Border _dropdownBorder() {
    return Border.all(
      color: const Color(
        0xffEAEAEA,
      ),
      width: 1,
    );
  }

  BoxShadow _dropDownShadow(ThemeData themeData) {
    return BoxShadow(
      color: themeData.colorScheme.inverseSurface,
      blurRadius: 30,
    );
  }
}

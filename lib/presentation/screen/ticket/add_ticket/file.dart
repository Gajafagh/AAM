import 'package:GajAfagh/data/remote/util/constant.dart';
import 'package:GajAfagh/util/extension.dart';
import 'package:flutter/material.dart';
import 'package:GajAfagh/presentation/component/dimension.dart';
import 'package:file_picker/file_picker.dart';
import 'package:GajAfagh/presentation/screen/ticket/add_ticket/upload_file.dart';
import 'package:GajAfagh/presentation/screen/ticket/add_ticket/add_ticket_screen.dart';
import 'package:GajAfagh/presentation/component/widget/small_widget.dart';
import 'package:GajAfagh/data/remote/api_service.dart';
import 'package:GajAfagh/data/remote/dio_api_service.dart';
import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:flutter/foundation.dart' show Uint8List, kIsWeb;

class FileComp extends StatefulWidget {
  final Widget image;
  final double height;
  final double width;
  static const double defaultHeight = 125;
  static const double defaultWidth = 189;

  const FileComp({
    Key? key,
    required this.image,
    this.height = defaultHeight,
    this.width = defaultWidth,
  }) : super(key: key);

  @override
  State<FileComp> createState() => FileComponent();
}

class FileComponent extends State<FileComp> {
  var title = 'جهت درج فایل کلیک کنید';

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);

    return InkWell(
      child: Container(
        width: widget.width,
        height: widget.height,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: themeData.colorScheme.surfaceVariant,
          border: _border(),
          boxShadow: [_fileShadow(themeData)],
          borderRadius: circular18,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [_titleText(themeData), sizedBoxH16, widget.image],
        ),
      ),
      onTap: () async {
        var files2 = null;
        var formData = null;

        // * Pick a File
        FilePickerResult? result = await FilePicker.platform
            .pickFiles(type: FileType.any, withData: true, allowMultiple: true);
        PlatformFile file = result!.files.first;
        String extension = file.name.split(".").last;

        if (kIsWeb) {
          formData = FormData.fromMap({
            "files": MultipartFile.fromBytes(result.files.first.bytes!,
                filename: result.files.first.name,
                contentType: MediaType("File", extension))
          });
        } else {
          files2 = result.paths
              .map((path) => MultipartFile.fromFileSync(path!))
              .toList();
          formData = FormData.fromMap({"files": files2});
        }

        setState(() {
          uploadingCont = true;
          withFile = 'true';
          title = file.name;

          // MultipartFile.fromBytes(
          //                 file.bytes!.cast(),
          //                 filename: file.name,
          //                 contentType: MediaType("File", extension),
          //               ),
          var perc = 0;
          var response = apiService.dio.post(RemoteConstant.uploadTicket,
              data: formData, onSendProgress: (received, total) async {
            if ((received / total * 100).floor() - perc >= 10 && perc < 90) {
              String text = "حجم آپلود شده : " + perc.toString() + "%";
              this.context.showSnackBar(text);
              perc = (received / total * 100).floor();
            }

            if (perc >= 90 && uploadingCont == true) {
              uploadingCont = false;
              withFile = 'true';
              ScaffoldMessenger.of(this.context).showSnackBar(
                const SnackBar(
                  content: Text("آپلود با موفقیت انجام شد"),
                ),
              );
            }
          });
          // var upl = ss.test(file.bytes!.cast(), file.name);
        });
      },
    );
  }

  Border _border() {
    return Border.all(
      color: const Color(
        0xffEAEAEA,
      ),
      width: 1,
    );
  }

  Text _titleText(ThemeData themeData) {
    return Text(
      title,
      style: themeData.textTheme.caption,
    );
  }

  BoxShadow _fileShadow(ThemeData themeData) {
    return BoxShadow(
      color: themeData.colorScheme.inverseSurface,
      blurRadius: 30,
    );
  }
}

import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:GajAfagh/data/remote/util/server_error.dart';
import 'package:GajAfagh/domain/model/ticket_user.dart';
import 'package:GajAfagh/domain/repository/ticket_user_repository.dart';

part 'list_ticket_event.dart';

part 'list_ticket_state.dart';

const allTicketsIndex = 1;
const pendingTicketsIndex = 0;
const answeredTicketsIndex = 2;
const myticketsIndex = 3;

class ListTicketBloc extends Bloc<ListTicketEvent, ListTicketState> {
  final TicketUserRepository repository;
  late List<TicketUserModel> allTickets;
  late List<TicketUserModel> pendingTickets;
  late List<TicketUserModel> answeredTickets;
  late List<TicketUserModel> myTickets;
  late int currentIndex;

  ListTicketBloc(this.repository) : super(ListTicketLoading()) {
    on<ListTicketEvent>(
      (event, emit) async {
        if (event is ListTicketStarted) {
          await handleStartedEvent(emit, event.isRefresh);
        } else if (event is ListTicketChangeStatus) {
          await handleChangeStatus(emit, event.index);
        } else if (event is ListTicketClickDeleteTicketButton) {
          await handleDeleteTask(emit, event.ticketId);
        } else if (event is ListTicketClickForwardTicketButton) {
          await handleForwardTask(emit, event.ticketId);
        } else if (event is ListTicketClickReplyTicketButton) {
          await handleReplyTask(emit, event.ticketId);
        }
      },
    );
  }

  Future<void> handleStartedEvent(
      Emitter<ListTicketState> emit, bool isRefresh) async {
    try {
      isRefresh
          ? emit(ListTicketLoadingForRefresh())
          : emit(ListTicketLoading());

      //getAllTicket
      pendingTickets = await repository
          .getAllTicketUserFiltered('pending')
          .then((value) => value.map((e) => e.toTicketUserModel()).toList())
          .timeout(const Duration(seconds: 10));
      answeredTickets = await repository
          .getAllTicketUserFiltered('answered')
          .then((value) => value.map((e) => e.toTicketUserModel()).toList())
          .timeout(const Duration(seconds: 10));

      allTickets = pendingTickets + answeredTickets;
      //pendingTickets = await repository
      //.getAllTicketUserFiltered('pending')
      //.then((value) => value.map((e) => e.toTicketUserModel()).toList());

      //answeredTickets = await repository
      //.getAllTicketUserFiltered('answered')
      //.then((value) => value.map((e) => e.toTicketUserModel()).toList());

      pendingTickets = await repository
          .getAllTicketUserFiltered('forwarded')
          .then((value) => value.map((e) => e.toTicketUserModel()).toList())
          .timeout(const Duration(seconds: 10));

      myTickets = await repository
          .getAllTicketUserFiltered('Mytickets')
          .then((value) => value.map((e) => e.toTicketUserModel()).toList())
          .timeout(const Duration(seconds: 10));

      //current index
      currentIndex = allTicketsIndex;

      //emit state
      emit(ListTicketSuccess(
          tickets: allTickets,
          sizePendingTickets: pendingTickets.length,
          sizeAllTickets: allTickets.length,
          sizeAnsweredTickets: answeredTickets.length,
          sizeMytickets: myTickets.length,
          selectedIndexToggleMenu: currentIndex));
    } catch (ex, stack) {
      print(ex);
      print('STDOUT ${stack}');
      emit(
        ListTicketError(
          ex is CustomError
              ? ex
              : CustomError(message: 'اتصال به اینترنت خود را بررسی کنید'),
        ),
      );
    }
  }

  Future<void> handleChangeStatus(
      Emitter<ListTicketState> emit, int index) async {
    final List<TicketUserModel> resultList = index == allTicketsIndex
        ? allTickets
        : index == pendingTicketsIndex
            ? pendingTickets
            : index == myticketsIndex
                ? myTickets
                : answeredTickets;

    //current index
    currentIndex = index;

    //emit state
    emit(ListTicketSuccess(
        tickets: resultList,
        sizePendingTickets: pendingTickets.length,
        sizeAllTickets: allTickets.length,
        sizeAnsweredTickets: answeredTickets.length,
        sizeMytickets: myTickets.length,
        selectedIndexToggleMenu: currentIndex));
  }

  Future<void> handleDeleteTask(
      Emitter<ListTicketState> emit, String ticketId) async {
    try {
      emit(ListTicketLoadingDeleteTask(ticketId));

      //check currentTab
      final List<TicketUserModel> resultList = currentIndex == allTicketsIndex
          ? allTickets
          : currentIndex == pendingTicketsIndex
              ? pendingTickets
              : currentIndex == myticketsIndex
                  ? myTickets
                  : pendingTickets;

      //request for delete item
      final resultDeleteItem = await repository
          .deleteTicket(ticketId)
          .timeout(const Duration(seconds: 10));

      if (resultDeleteItem) {
        //remove in list

        //emit success
        await handleStartedEvent(emit, true);
      }
    } catch (ex) {
      print(ex);
      emit(
        ListTicketError(
          ex is CustomError
              ? ex
              : CustomError(message: 'اتصال به اینترنت خود را بررسی کنید'),
        ),
      );
    }
  }

  Future<void> handleForwardTask(
      Emitter<ListTicketState> emit, String ticketId) async {
    try {
      emit(ListTicketForwardTask(ticketId));

      //request for delete item
      final forwardTicket = await repository
          .forwardTicket(ticketId)
          .timeout(const Duration(seconds: 10));
    } catch (ex) {
      print(ex);
      emit(
        ListTicketError(
          ex is CustomError
              ? ex
              : CustomError(message: 'اتصال به اینترنت خود را بررسی کنید'),
        ),
      );
    }
  }

  Future<void> handleReplyTask(
      Emitter<ListTicketState> emit, String ticketId) async {
    try {
      emit(ListTicketReplyTask(ticketId));

      //request for delete item
      final forwardTicket = await repository
          .replyTicket(ticketId)
          .timeout(const Duration(seconds: 10));
    } catch (ex) {
      print(ex);
      emit(
        ListTicketError(
          ex is CustomError
              ? ex
              : CustomError(message: 'اتصال به اینترنت خود را بررسی کنید'),
        ),
      );
    }
  }
}

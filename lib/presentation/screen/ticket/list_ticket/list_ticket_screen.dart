import 'package:easy_search_bar/easy_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:GajAfagh/domain/model/ticket_user.dart';
import 'package:GajAfagh/domain/repository/ticket_user_repository.dart';
import 'package:GajAfagh/gen/assets.gen.dart';
import 'package:GajAfagh/presentation/component/dimension.dart';
import 'package:GajAfagh/presentation/component/widget/elevated_textfield.dart';
import 'package:GajAfagh/presentation/component/widget/empty_view.dart';
import 'package:GajAfagh/presentation/component/widget/small_widget.dart';
import 'package:GajAfagh/presentation/screen/home/appbar.dart';
import 'package:GajAfagh/presentation/screen/home/home_route.dart';
import 'package:GajAfagh/presentation/screen/ticket/add_ticket/drop_down.dart';
import 'package:GajAfagh/presentation/screen/ticket/list_ticket/bloc/list_ticket_bloc.dart';
import 'package:GajAfagh/presentation/screen/ticket/list_ticket/toggle_group/custom_toggle_group.dart';
import 'package:GajAfagh/presentation/screen/ticket/list_ticket/unanswered_ticket_item.dart';
import 'package:GajAfagh/presentation/screen/ticket/list_ticket/answered_ticket_item.dart';
import 'package:GajAfagh/presentation/color.dart';

class ListTicketScreen extends StatefulWidget {
  static const String route = '/list';
  static const _listTicketPadding =
      56 + (HomeRoute.bottomNavHeight - HomeRoute.bottomNavContainerHeight);
  static const int defaultToggleMenuSelected = 1;
  static const String defaultDate = '             ';

  const ListTicketScreen({Key? key}) : super(key: key);

  @override
  State<ListTicketScreen> createState() => _ListTicketScreenState();
}

class _ListTicketScreenState extends State<ListTicketScreen> {
  final RefreshController _refreshController = RefreshController();
  final GetIt instance = GetIt.instance;

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    return HomeRoute(
        child: Column(children: [
      BlocProvider<ListTicketBloc>(
        create: (context) => ListTicketBloc(
          instance<TicketUserRepository>(),
        )..add(const ListTicketStarted()),
        child: BlocConsumer<ListTicketBloc, ListTicketState>(
          listenWhen: (p, c) => c is ListTicketSuccess || c is ListTicketError,
          listener: (context, state) {
            if (state is ListTicketSuccess) {
              _refreshController.refreshCompleted();
            } else if (state is ListTicketError) {
              _refreshController.refreshFailed();
            }
          },
          buildWhen: (p, c) =>
              c is ListTicketLoading ||
              c is ListTicketSuccess ||
              c is ListTicketError,
          builder: (context, state) {
            //success state
            if (state is ListTicketSuccess) {
              return _successState(themeData, state, context);
            }
            //loading state
            else if (state is ListTicketLoading) {
              return _loadingState(themeData);
            }
            //error state
            else if (state is ListTicketError) {
              return _errorState(state);
            } else {
              throw Exception('state is not supported!');
            }
          },
        ),
      ),
    ]));
  }

  Widget _errorState(ListTicketError state) {
    print(state.error);
    return Center(
      child: Text(state.error.message),
    );
  }

  Widget _loadingState(ThemeData themeData) {
    return Center(
      child: showLoading(
        themeData.colorScheme.primary,
      ),
    );
  }

  Widget _successState(
      ThemeData themeData, ListTicketSuccess state, BuildContext context) {
    return SmartRefresher(
      controller: _refreshController,
      header: _refreshHeader(),
      onRefresh: () {
        context.read<ListTicketBloc>().add(
              const ListTicketStarted(isRefresh: true),
            );
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          sizedBoxH24,

          //appbar
          const Padding(
            padding: padding36H,
            child: HomeAppBar(),
          ),

          sizedBoxH48,

          Container(
            child: Scaffold(
                appBar: EasySearchBar(
              title: const Text('جستوجو'),
              onSearch: (value) => setState(() => _listTickets == value),
            )),
          ),

          //titleText
          Padding(
            padding: padding32H,
            child: _largeText(themeData, 'تیکت های شما'),
          ),

          sizedBoxH32,

          //toggleGroup
          Padding(
            padding: padding24H,
            child: CustomToggleGroup(
              selectedIndex: state.selectedIndexToggleMenu,
              sizeAnsweredTickets: state.sizeAnsweredTickets,
              sizeAllTickets: state.sizeAllTickets,
              sizePendingTickets: state.sizePendingTickets,
              sizeMyTickets: state.sizeMytickets,
            ),
          ),

          sizedBoxH16,

          //searchTextField
          if (state.tickets.isNotEmpty) sizedBoxH12,

          //list ticket
          Expanded(
            child: state.tickets.isNotEmpty
                ? _listTickets(state.tickets)
                : _emptyView(),
          ),
        ],
      ),
    );
  }

  ClassicHeader _refreshHeader() {
    return ClassicHeader(
      completeText: 'با موفقیت انجام شد',
      refreshingText: 'در حال بروزرسانی',
      idleText: 'برای بروزرسانی پایین بکشید',
      failedText: 'خطای نامشخص',
      releaseText: 'رها کنید',
      spacing: 8,
      textStyle:
          Theme.of(context).textTheme.bodyText2!.apply(color: Colors.grey),
    );
  }

  Widget _listTickets(List<TicketUserModel> lists) {
    return ListView.builder(
      padding:
          const EdgeInsets.only(bottom: ListTicketScreen._listTicketPadding),
      itemBuilder: (context, index) {
        final TicketUserModel ticket = lists[index];
        if (ticket.status == 'forwarded') {
          List adminMsg = ticket.type.split("|");
          return AnsweredTicketItem(
            ticketTitle: ticket.title,
            ticketType: ticket.type,
            ticketDesc: ticket.desc,
            ticketDate: ticket.date,
            adminMessage: adminMsg[2],
            ticketId: ticket.id,
            senderOf: ticket.sender,
            withFile: ticket.withFile,
            ticketTypeColor: LightColorPalette.blackTextColor,
            closed: ticket.closed,
            ticketNumber: ticket.ticketNumber,
          );
        } else if (ticket.status == 'answered') {
          List adminMsg = ticket.type.split("|");

          return AnsweredTicketItem(
            ticketTitle: ticket.title,
            ticketType: ticket.type,
            ticketDesc: ticket.desc,
            ticketId: ticket.id,
            ticketDate: ticket.date,
            adminMessage: adminMsg[2],
            withFile: ticket.withFile,
            senderOf: ticket.sender,
            ticketTypeColor: LightColorPalette.blackTextColor,
            closed: ticket.closed,
            ticketNumber: ticket.ticketNumber,
          );
        } else if (ticket.status == 'pending') {
          return UnansweredTicketItem(
              ticketTitle: ticket.title,
              ticketType: ticket.type,
              ticketDesc: ticket.desc,
              ticketDate: ticket.date,
              ticketId: ticket.id,
              ticketTypeColor: LightColorPalette.blackTextColor,
              sender: ticket.sender,
              withFile: ticket.withFile,
              seen: ticket.seen,
              closed: ticket.closed,
              ticketNumber: ticket.ticketNumber);
        } else {
          return UnansweredTicketItem(
            ticketTitle: ticket.title,
            ticketType: ticket.type,
            ticketDesc: ticket.desc,
            ticketDate: ticket.date,
            ticketId: ticket.id,
            ticketTypeColor: LightColorPalette.blackTextColor,
            sender: ticket.sender,
            withFile: ticket.withFile,
            seen: "seen",
            closed: ticket.closed,
            ticketNumber: ticket.ticketNumber,
          );
        }
      },
      itemCount: lists.length,
    );
  }

  Widget _largeText(ThemeData themeData, String text) {
    return Text(
      text,
      style: themeData.textTheme.headline2,
    );
  }

  Widget _emptyView() {
    return EmptyView(
      message: 'چیزی برای نمایش وجود ندارد',
      image: Assets.image.svg.empty.svg(width: 150),
    );
  }
}

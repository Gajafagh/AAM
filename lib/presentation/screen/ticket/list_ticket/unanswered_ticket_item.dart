// import 'dart:convert';
// import 'dart:html' as html;
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:GajAfagh/gen/assets.gen.dart';
import 'package:GajAfagh/presentation/component/dimension.dart';
import 'package:GajAfagh/presentation/screen/ticket/list_ticket/bloc/list_ticket_bloc.dart';
import 'package:GajAfagh/presentation/screen/ticket/list_ticket/download.dart';
import 'package:GajAfagh/util/extension.dart';
import 'package:GajAfagh/presentation/screen/ticket/add_ticket/answer_ticket.dart';
import 'package:GajAfagh/presentation/screen/ticket/add_ticket/reply_ticket.dart';
import 'package:GajAfagh/presentation/color.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

// void downloadFileFromDownloadableLink(String url) {
//   final Random random = Random();
//   var randid = random.nextInt(10000);
//   html.AnchorElement anchorElement = html.AnchorElement(href: url);
//   anchorElement.download = "$randid.${url.split("-").last}";
//   anchorElement.click();
// }

class UnansweredTicketItem extends StatelessWidget {
  final String ticketTitle;
  final String ticketId;
  final String ticketType;
  final String sender;
  final List withFile;
  final Color ticketTypeColor;
  final String ticketDesc;
  final String ticketDate;
  final String seen;
  final String closed;
  final String ticketNumber;

  const UnansweredTicketItem(
      {Key? key,
      required this.ticketTitle,
      required this.ticketType,
      required this.sender,
      required this.withFile,
      required this.ticketDesc,
      required this.ticketDate,
      required this.ticketTypeColor,
      required this.ticketId,
      required this.seen,
      required this.closed,
      required this.ticketNumber})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(closed);
    final ThemeData themeData = Theme.of(context);
    return Container(
      padding: padding18,
      margin: const EdgeInsets.only(bottom: 12, top: 12) + padding32H,
      decoration: BoxDecoration(
        color: themeData.colorScheme.surfaceVariant,
        borderRadius: circular16,
        border: Border.all(
          color: const Color(
            0xffE6E6E6,
          ),
          width: 0.5,
        ),
        boxShadow: [
          BoxShadow(
            color: themeData.colorScheme.inverseSurface,
            blurRadius: 10,
          )
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _titleText(themeData),
          sizedBoxH20,
          _descText(themeData),
          sizedBoxH48,
          Row(children: [
            _forwardButton(themeData),
            const Spacer(),
            _fileButton(themeData),
            const Spacer(),
            if (sender != "مدیریت" && closed == " ") _replyButton(themeData)
          ]),
          sizedBoxH48,
          Row(
            children: [
              const Spacer(),
              if (closed == " ")
                _deleteButton(themeData)
              else
                _closeText(themeData),
              const Spacer(),
            ],
          ),
        ],
      ),
    );
  }

  Widget _closeText(ThemeData themeData) {
    return InkWell(
        child: Text(
      "\nتیکت توسط " + closed + " بسته شده است!\n",
      style: themeData.textTheme.bodyText2!.copyWith(
          color: themeData.colorScheme.error,
          fontWeight: FontWeight.w700,
          fontSize: 10),
    ));
  }

  _dateText(ThemeData themeData) {
    if (seen == "unseen") {
      return TextSpan(
        text: "\n\nبازدید : ❌",
        style: themeData.textTheme.bodyText2!.copyWith(
          color: themeData.colorScheme.error,
          fontWeight: FontWeight.w700,
        ),
      );
    } else {
      return TextSpan(
        text: "\n\nبازدید : ✅",
        style: themeData.textTheme.bodyText2!.copyWith(
          color: themeData.colorScheme.primary,
          fontWeight: FontWeight.w700,
        ),
      );
    }
  }

  Widget _deleteButton(ThemeData themeData) {
    return Material(
      color: Colors.transparent,
      child: BlocBuilder<ListTicketBloc, ListTicketState>(
        builder: (context, state) {
          return InkWell(
            borderRadius: circular10,
            highlightColor: themeData.colorScheme.error.withOpacity(0.2),
            onTap: () {
              if (state is ListTicketSuccess) {
                context
                    .read<ListTicketBloc>()
                    .add(ListTicketClickDeleteTicketButton(ticketId));
              }
            },
            child:
                (state is ListTicketLoadingDeleteTask && state.id == ticketId)
                    ? SizedBox(
                        width: 36,
                        child: CupertinoActivityIndicator(
                          color: themeData.colorScheme.error.withOpacity(0.5),
                        ),
                      )
                    : Container(
                        padding: padding9H + padding3V,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: themeData.colorScheme.error,
                            width: 0.7,
                          ),
                          borderRadius: circular10,
                        ),
                        child: Row(
                          children: [
                            Assets.image.svg.delete.svg(),
                            sizedBoxW2,
                            Text(
                              'بستن تیکت',
                              style: themeData.textTheme.caption!.copyWith(
                                color: themeData.colorScheme.error,
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                              ),
                            )
                          ],
                        ),
                      ),
          );
        },
        buildWhen: (p, c) =>
            (c is ListTicketLoadingDeleteTask || c is ListTicketSuccess),
      ),
    );
  }

  Widget _forwardButton(ThemeData themeData) {
    return Material(
      color: Colors.transparent,
      child: BlocBuilder<ListTicketBloc, ListTicketState>(
        builder: (context, state) {
          return InkWell(
            borderRadius: circular10,
            highlightColor: themeData.colorScheme.error.withOpacity(0.2),
            onTap: () {
              // context
              //     .read<ListTicketBloc>()
              //     .add(ListTicketClickForwardTicketButton(ticketId));
              // Navigator.pushNamed(context, AnswerTicketScreen.route);
              context
                  .showSnackBar('قابلیت فروارد فقط برای مدیران در دسترس است!!');
            },
            child:
                (state is ListTicketLoadingDeleteTask && state.id == ticketId)
                    ? SizedBox(
                        width: 40,
                        child: CupertinoActivityIndicator(
                          color: themeData.colorScheme.error.withOpacity(0.5),
                        ),
                      )
                    : Container(
                        padding: padding9H + padding3V,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: themeData.colorScheme.primary,
                            width: 0.7,
                          ),
                          borderRadius: circular10,
                        ),
                        child: Row(
                          children: [
                            Assets.image.svg.arrow.svg(),
                            sizedBoxW2,
                            Text(
                              'ارسال به',
                              style: themeData.textTheme.caption!.copyWith(
                                color: themeData.colorScheme.primary,
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                              ),
                            )
                          ],
                        ),
                      ),
          );
        },
        buildWhen: (p, c) =>
            (c is ListTicketLoadingDeleteTask || c is ListTicketSuccess),
      ),
    );
  }

  Widget _replyButton(ThemeData themeData) {
    return Material(
      color: Colors.transparent,
      child: BlocBuilder<ListTicketBloc, ListTicketState>(
        builder: (context, state) {
          return InkWell(
            borderRadius: circular10,
            highlightColor: themeData.colorScheme.error.withOpacity(0.2),
            onTap: () {
              context
                  .read<ListTicketBloc>()
                  .add(ListTicketClickReplyTicketButton(ticketId));
              Navigator.pushNamed(context, ReplyTicketScreen.route);
            },
            child:
                (state is ListTicketLoadingDeleteTask && state.id == ticketId)
                    ? SizedBox(
                        width: 36,
                        child: CupertinoActivityIndicator(
                          color: themeData.colorScheme.error.withOpacity(0.5),
                        ),
                      )
                    : Container(
                        padding: padding9H + padding3V,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: themeData.colorScheme.error,
                            width: 0.7,
                          ),
                          borderRadius: circular10,
                        ),
                        child: Row(
                          children: [
                            Assets.image.svg.arrow.svg(),
                            sizedBoxW2,
                            Text(
                              'پاسخ',
                              style: themeData.textTheme.caption!.copyWith(
                                color: themeData.colorScheme.error,
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                              ),
                            )
                          ],
                        ),
                      ),
          );
        },
        buildWhen: (p, c) =>
            (c is ListTicketLoadingDeleteTask || c is ListTicketSuccess),
      ),
    );
  }

  Widget _fileButton(ThemeData themeData) {
    if (withFile.isNotEmpty) {
      return Material(
        color: Colors.transparent,
        child: BlocBuilder<ListTicketBloc, ListTicketState>(
          builder: (context, state) {
            return InkWell(
              borderRadius: circular10,
              highlightColor: themeData.colorScheme.error.withOpacity(0.2),
              onTap: () async {
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text("... بارگیری آغاز شد!"),
                  ),
                );
                // if (kIsWeb) {
                //   withFile.forEach(
                //       (element) => downloadFileFromDownloadableLink(element));
                // } else {
                var downloader_state = FileDownloaderState(imgUrl: withFile);
                downloader_state.downloadFile(context);
                // }`
              },
              child:
                  (state is ListTicketLoadingDeleteTask && state.id == ticketId)
                      ? SizedBox(
                          width: 36,
                          child: CupertinoActivityIndicator(
                            color: themeData.colorScheme.error.withOpacity(0.5),
                          ),
                        )
                      : Container(
                          padding: padding9H + padding3V,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: themeData.colorScheme.error,
                              width: 0.7,
                            ),
                            borderRadius: circular10,
                          ),
                          child: Row(
                            children: [
                              Assets.image.svg.arrow.svg(),
                              sizedBoxW2,
                              Text(
                                'دانلود ضمیمه',
                                style: themeData.textTheme.caption!.copyWith(
                                  color: themeData.colorScheme.primary,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 15,
                                ),
                              )
                            ],
                          ),
                        ),
            );
          },
          buildWhen: (p, c) =>
              (c is ListTicketLoadingDeleteTask || c is ListTicketSuccess),
        ),
      );
    } else {
      return Material(
        color: Colors.transparent,
        child: BlocBuilder<ListTicketBloc, ListTicketState>(
          builder: (context, state) {
            return InkWell(
              borderRadius: circular10,
              highlightColor: themeData.colorScheme.error.withOpacity(0.2),
              onTap: () {},
              child:
                  (state is ListTicketLoadingDeleteTask && state.id == ticketId)
                      ? SizedBox(
                          width: 36,
                          child: CupertinoActivityIndicator(
                            color: themeData.colorScheme.error.withOpacity(0.5),
                          ),
                        )
                      : Container(),
            );
          },
          buildWhen: (p, c) =>
              (c is ListTicketLoadingDeleteTask || c is ListTicketSuccess),
        ),
      );
    }
  }

  Widget _titleText(ThemeData themeData) {
    return RichText(
      text: TextSpan(children: [
        TextSpan(
          text: ' موضوع : ',
          style: themeData.textTheme.bodyText2!.copyWith(
            color: LightColorPalette.blackTextColor,
            fontWeight: FontWeight.w700,
          ),
        ),
        TextSpan(
          text: ticketTitle + "\n",
          style: themeData.textTheme.bodyText2!.copyWith(
            color: themeData.colorScheme.primary,
            fontWeight: FontWeight.w700,
          ),
        ),
        TextSpan(
          text: ' به : ',
          style: themeData.textTheme.bodyText2!.copyWith(
            color: LightColorPalette.blackTextColor,
            fontWeight: FontWeight.w700,
          ),
        ),
        TextSpan(
          text: ' $ticketType \n',
          style: themeData.textTheme.bodyText2!.copyWith(
            color: themeData.colorScheme.primary,
            fontWeight: FontWeight.w700,
          ),
        ),
        TextSpan(
          text: ' از طرف : ',
          style: themeData.textTheme.bodyText2!.copyWith(
            color: LightColorPalette.blackTextColor,
            fontWeight: FontWeight.w700,
          ),
        ),
        TextSpan(
          text: ' $sender',
          style: themeData.textTheme.bodyText2!.copyWith(
            color: themeData.colorScheme.primary,
            fontWeight: FontWeight.w700,
          ),
        ),
        TextSpan(
          text: '\n تاریخ: ',
          style: themeData.textTheme.bodyText2!.copyWith(
            color: ticketTypeColor,
            fontWeight: FontWeight.w700,
          ),
        ),
        TextSpan(
          text: ticketDate,
          style: themeData.textTheme.bodyText2!.copyWith(
            color: themeData.colorScheme.primary,
            fontWeight: FontWeight.w700,
          ),
        ),
        TextSpan(
          text: '\n شماره نامه: ',
          style: themeData.textTheme.bodyText2!.copyWith(
            color: ticketTypeColor,
            fontWeight: FontWeight.w700,
          ),
        ),
        TextSpan(
          text: ticketNumber,
          style: themeData.textTheme.bodyText2!.copyWith(
            color: themeData.colorScheme.primary,
            fontWeight: FontWeight.w700,
          ),
        ),
        _dateText(themeData)
      ]),
    );
  }

  Widget _descText(ThemeData themeData) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      primary: false,
      child: Text(
        "\n" + ticketDesc,
        style: themeData.textTheme.caption!.copyWith(fontSize: 16),
      ),
    );
  }
}

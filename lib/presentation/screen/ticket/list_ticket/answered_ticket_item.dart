import 'package:GajAfagh/presentation/screen/ticket/list_ticket/unanswered_ticket_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:GajAfagh/presentation/component/dimension.dart';
import 'package:GajAfagh/util/extension.dart';
import 'package:GajAfagh/gen/assets.gen.dart';
import 'package:GajAfagh/presentation/screen/ticket/list_ticket/download.dart';
import 'package:GajAfagh/presentation/screen/ticket/list_ticket/bloc/list_ticket_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:GajAfagh/presentation/screen/ticket/add_ticket/reply_ticket.dart';

class AnsweredTicketItem extends StatelessWidget {
  final String ticketTitle;
  final String ticketId;
  final String ticketType;
  final String senderOf;
  final Color ticketTypeColor;
  final String ticketDesc;
  final String ticketDate;
  final String adminMessage;
  final List withFile;
  final String closed;
  final String ticketNumber;

  const AnsweredTicketItem(
      {Key? key,
      required this.ticketTitle,
      required this.ticketType,
      required this.ticketId,
      required this.senderOf,
      required this.ticketTypeColor,
      required this.ticketDesc,
      required this.ticketDate,
      required this.withFile,
      required this.adminMessage,
      required this.closed,
      required this.ticketNumber})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    return Container(
      padding: padding18,
      margin: const EdgeInsets.only(bottom: 12, top: 12) + padding32H,
      decoration: BoxDecoration(
        color: themeData.colorScheme.surfaceVariant,
        borderRadius: circular16,
        border: Border.all(
          color: const Color(
            0xffE6E6E6,
          ),
          width: 1.5,
        ),
        boxShadow: [
          BoxShadow(
            color: themeData.colorScheme.inverseSurface,
            blurRadius: 10,
          )
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _titleText(themeData),
              // _dateText(themeData),
            ],
          ),
          sizedBoxH20,
          _textContainer2(themeData, ticketDesc),
          for (var i = 1; i < ticketType.split("|").length; i = i + 2)
            Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _adminText(themeData, i),
                ],
              ),
              sizedBoxH14,
              _textContainer(themeData, i),
              sizedBoxH14,
            ]),
          Row(children: [
            if (senderOf != "مدیریت" && closed == " ")
              _replyButton(context, themeData),
            const Spacer(),
            _fileButton(themeData, context),
            const Spacer(),
          ]),
          sizedBoxH48,
          Row(
            children: [
              const Spacer(),
              if (closed == " ")
                _deleteButton(themeData)
              else
                _closeText(themeData),
              const Spacer(),
            ],
          ),
        ],
      ),
    );
  }

  Widget _closeText(ThemeData themeData) {
    return InkWell(
        child: Text(
      "\nتیکت توسط " + closed + " بسته شده است!\n",
      style: themeData.textTheme.bodyText2!.copyWith(
          color: themeData.colorScheme.error,
          fontWeight: FontWeight.w700,
          fontSize: 10),
    ));
  }

  Widget _titleText(ThemeData themeData) {
    var to = ticketType.split("|")[0];
    var sender = ticketType.split("|")[1];

    return Expanded(
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: ' موضوع: ',
              style: themeData.textTheme.bodyText2!.copyWith(
                color: ticketTypeColor,
                fontWeight: FontWeight.w700,
              ),
            ),
            TextSpan(
              text: ticketTitle,
              style: themeData.textTheme.bodyText2!.copyWith(
                color: themeData.colorScheme.primary,
                fontWeight: FontWeight.w700,
              ),
            ),
            TextSpan(
              text: '\n از طرف: ',
              style: themeData.textTheme.bodyText2!.copyWith(
                color: ticketTypeColor,
                fontWeight: FontWeight.w700,
              ),
            ),
            TextSpan(
              text: senderOf,
              style: themeData.textTheme.bodyText2!.copyWith(
                color: themeData.colorScheme.primary,
                fontWeight: FontWeight.w700,
              ),
            ),
            TextSpan(
              text: '\n به: ',
              style: themeData.textTheme.bodyText2!.copyWith(
                color: ticketTypeColor,
                fontWeight: FontWeight.w700,
              ),
            ),
            TextSpan(
              text: to,
              style: themeData.textTheme.bodyText2!.copyWith(
                color: themeData.colorScheme.primary,
                fontWeight: FontWeight.w700,
              ),
            ),
            TextSpan(
              text: '\n تاریخ: ',
              style: themeData.textTheme.bodyText2!.copyWith(
                color: ticketTypeColor,
                fontWeight: FontWeight.w700,
              ),
            ),
            TextSpan(
              text: ticketDate,
              style: themeData.textTheme.bodyText2!.copyWith(
                color: themeData.colorScheme.primary,
                fontWeight: FontWeight.w700,
              ),
            ),
            TextSpan(
              text: '\n شماره نامه: ',
              style: themeData.textTheme.bodyText2!.copyWith(
                color: ticketTypeColor,
                fontWeight: FontWeight.w700,
              ),
            ),
            TextSpan(
              text: ticketNumber,
              style: themeData.textTheme.bodyText2!.copyWith(
                color: themeData.colorScheme.primary,
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _adminText(ThemeData themeData, int i) {
    var answers = ticketType.split("|");
    var nameof = answers[i];
    return Expanded(
      child: RichText(
        text: TextSpan(children: [
          TextSpan(
            text: ' پاسخ از: ',
            style: themeData.textTheme.bodyText2!.copyWith(
              color: ticketTypeColor,
              fontWeight: FontWeight.w700,
            ),
          ),
          TextSpan(
            text: nameof,
            style: themeData.textTheme.bodyText2!.copyWith(
              color: themeData.colorScheme.primary,
              fontWeight: FontWeight.w700,
            ),
          ),
        ]),
      ),
    );
  }

  Widget _replyButton(BuildContext context, ThemeData themeData) {
    return Material(
        color: Colors.transparent,
        child: InkWell(
          borderRadius: circular10,
          highlightColor: themeData.colorScheme.error.withOpacity(0.2),
          onTap: () {
            context
                .read<ListTicketBloc>()
                .add(ListTicketClickReplyTicketButton(ticketId));
            Navigator.pushNamed(context, ReplyTicketScreen.route);
          },
          child: Container(
            padding: padding9H + padding3V,
            decoration: BoxDecoration(
              border: Border.all(
                color: themeData.colorScheme.error,
                width: 0.7,
              ),
              borderRadius: circular10,
            ),
            child: Row(
              children: [
                Assets.image.svg.arrow.svg(),
                sizedBoxW2,
                Text(
                  'پاسخ',
                  style: themeData.textTheme.caption!.copyWith(
                    color: themeData.colorScheme.primary,
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                  ),
                )
              ],
            ),
          ),
        ));
  }

  Widget _fileButton(ThemeData themeData, BuildContext context) {
    print(withFile);
    if (withFile.isNotEmpty) {
      return Material(
          color: Colors.transparent,
          child: InkWell(
            borderRadius: circular10,
            highlightColor: themeData.colorScheme.error.withOpacity(0.2),
            onTap: () {
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  content: Text("... بارگیری آغاز شد!"),
                ),
              );
              // if (kIsWeb) {
              //   withFile.forEach(
              //       (element) => downloadFileFromDownloadableLink(element));
              // } else {
              var downloader_state = FileDownloaderState(imgUrl: withFile);
              downloader_state.downloadFile(context);

              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  content: Text(
                      "دانلود با موفقیت انجام شد - مسیر ذخیره: مدیریت فایل، پوشه Downloads"),
                ),
              );
              // }
            },
            child: Container(
              padding: padding9H + padding3V,
              decoration: BoxDecoration(
                border: Border.all(
                  color: themeData.colorScheme.error,
                  width: 0.7,
                ),
                borderRadius: circular10,
              ),
              child: Row(
                children: [
                  Assets.image.svg.arrow.svg(),
                  sizedBoxW2,
                  Text(
                    'دانلود ضمیمه',
                    style: themeData.textTheme.caption!.copyWith(
                      color: themeData.colorScheme.primary,
                      fontWeight: FontWeight.w500,
                      fontSize: 15,
                    ),
                  )
                ],
              ),
            ),
          ));
    } else {
      return Material(
        color: Colors.transparent,
      );
    }
  }

  Widget _dateText(ThemeData themeData) {
    return Padding(
      padding: padding4L,
      child: Text(
        ticketDate.toFaNumber(),
        style: themeData.textTheme.caption!.copyWith(
          fontSize: 10,
          color: themeData.colorScheme.onSurface,
        ),
      ),
    );
  }

  Widget _textContainer(ThemeData themeData, int i, [double height = 115]) {
    var answers = ticketType.split("|");
    var message = answers[i + 1];

    return Container(
      alignment: Alignment.centerRight,
      height: height,
      padding: padding12,
      decoration: BoxDecoration(
        border: Border.all(
          width: 2,
          color: Color.fromARGB(255, 99, 255, 177),
        ),
        borderRadius: circular12,
      ),
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        primary: false,
        child: Text(
          message,
          style: themeData.textTheme.caption!.copyWith(fontSize: 14),
        ),
      ),
    );
  }

  Widget _deleteButton(ThemeData themeData) {
    return Material(
      color: Colors.transparent,
      child: BlocBuilder<ListTicketBloc, ListTicketState>(
        builder: (context, state) {
          return InkWell(
            borderRadius: circular10,
            highlightColor: themeData.colorScheme.error.withOpacity(0.2),
            onTap: () {
              if (state is ListTicketSuccess) {
                context
                    .read<ListTicketBloc>()
                    .add(ListTicketClickDeleteTicketButton(ticketId));
              }
            },
            child:
                (state is ListTicketLoadingDeleteTask && state.id == ticketId)
                    ? SizedBox(
                        width: 36,
                        child: CupertinoActivityIndicator(
                          color: themeData.colorScheme.error.withOpacity(0.5),
                        ),
                      )
                    : Container(
                        padding: padding9H + padding3V,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: themeData.colorScheme.error,
                            width: 0.7,
                          ),
                          borderRadius: circular10,
                        ),
                        child: Row(
                          children: [
                            Assets.image.svg.delete.svg(),
                            sizedBoxW2,
                            Text(
                              'بستن تیکت',
                              style: themeData.textTheme.caption!.copyWith(
                                color: themeData.colorScheme.error,
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                              ),
                            )
                          ],
                        ),
                      ),
          );
        },
        buildWhen: (p, c) =>
            (c is ListTicketLoadingDeleteTask || c is ListTicketSuccess),
      ),
    );
  }

  Widget _textContainer2(ThemeData themeData, String text,
      [double height = 115]) {
    return Container(
      alignment: Alignment.center,
      height: height,
      padding: padding12,
      decoration: BoxDecoration(
        border: Border.all(
          width: 2,
          color: Color.fromARGB(255, 255, 142, 142),
        ),
        borderRadius: circular12,
      ),
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        primary: false,
        child: Text(
          text,
          style: themeData.textTheme.caption!.copyWith(fontSize: 14),
        ),
      ),
    );
  }
}

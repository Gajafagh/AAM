import 'dart:io';
import 'package:GajAfagh/util/extension.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'package:file_utils/file_utils.dart';
import 'dart:math';
import 'package:permission_handler/permission_handler.dart';
import 'package:GajAfagh/presentation/screen/ticket/list_ticket/list_ticket_screen.dart';
// class FileDownloader extends StatefulWidget {
//   final String imgUrl;

//   const FileDownloader({Key? key, required this.imgUrl}) : super(key: key);
//   @override
//   FileDownloaderState createState() => FileDownloaderState();
// }

class FileDownloaderState extends StatelessWidget {
  bool downloading = false;
  var progress = 0;
  var path = "No Data";
  var platformVersion = "Unknown";
  var _onPressed;
  static final Random random = Random();
  final List imgUrl;
  Directory? externalDir;

  FileDownloaderState({Key? key, required this.imgUrl}) : super(key: key);

  // @override
  // void initState() {
  //   super.initState();
  //   downloadFile();
  // }

  Future<void> downloadFile(BuildContext context) async {
    Dio dio = Dio();

    String dirloc = "";
    if (Platform.isAndroid) {
      dirloc = "/sdcard/Download/";
    } else {
      dirloc = (await getApplicationDocumentsDirectory()).path;
    }

    var randid = random.nextInt(10000);
    final status = await Permission.storage.request();
    if (status.isGranted) {
      //try {
      FileUtils.mkdir([dirloc]);

      var num = 1;
      var allFiles = imgUrl.length.toString();
      for (var i = 0; i < imgUrl.length; i++) {
        await dio.download(imgUrl[i],
            dirloc + randid.toString() + "." + imgUrl[i].split(".").last,
            onReceiveProgress: (receivedBytes, totalBytes) {
          if ((receivedBytes / totalBytes * 100).floor() - progress >= 10 &&
              progress < 90 &&
              num <= imgUrl.length) {
            downloading = true;
            progress = ((receivedBytes / totalBytes) * 100).floor();
            context.showSnackBar("فایل : $num / $allFiles | " +
                progress.toString() +
                "% از دریافت فایل انجام شده است! ");
          }
          if (progress >= 90 && downloading == true) {
            num = num + 1;
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                content: Text(
                    "دانلود با موفقیت انجام شد - مسیر ذخیره: مدیریت فایل، پوشه Downloads"),
              ),
            );
            downloading = false;
            progress = 0;
          }
        });
      }
      ;

      //} catch (e) {
      //print(e);
    }
  }

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(
        title: Text('File Downloader'),
      ),
      body: Center(
          child: downloading
              ? Container(
                  height: 120.0,
                  width: 200.0,
                  child: Card(
                    color: Colors.black,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CircularProgressIndicator(),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          'Downloading File: $progress',
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                )
              : Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(path),
                    MaterialButton(
                      child: Text('Request Permission Again.'),
                      onPressed: _onPressed,
                      disabledColor: Colors.blueGrey,
                      color: Colors.pink,
                      textColor: Colors.white,
                      height: 40.0,
                      minWidth: 100.0,
                    ),
                  ],
                )));
}

import 'dart:ui';

import 'package:GajAfagh/presentation/screen/home/home.dart';
import 'package:GajAfagh/presentation/screen/home/home_route.dart';
import 'package:GajAfagh/presentation/screen/note/screens/notes_screen.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_clander/clander.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:GajAfagh/domain/repository/auth_repository.dart';
import 'package:GajAfagh/domain/repository/ticket_user_repository.dart';
import 'package:GajAfagh/presentation/component/dimension.dart';
import 'package:GajAfagh/presentation/component/widget/small_widget.dart';
import 'package:GajAfagh/presentation/screen/auth/login_screen.dart';
import 'package:GajAfagh/presentation/screen/home/appbar.dart';
import 'package:GajAfagh/presentation/screen/home/exit_dialog.dart';
import 'package:GajAfagh/presentation/screen/home/menu_item.dart';
import 'package:GajAfagh/presentation/screen/ticket/add_ticket/add_ticket_screen.dart';
import 'bloc/home_bloc.dart';

class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}

class PushNotification {
  PushNotification({
    this.title,
    this.body,
  });

  String? title;
  String? body;
}

class TodoScreen extends StatefulWidget {
  static const String route = '/todo-home';
  static const String item1 = 'خانه';
  static const String item2 = 'لیست وظایف';
  static const String item3 = 'تقویم';
  static const String item4 = 'یادداشت ها';
  static const String item5 = 'راهنما';
  static const String item6 = 'بازگشت به تیکت';
  const TodoScreen({Key? key}) : super(key: key);

  @override
  State<TodoScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<TodoScreen> {
  final getIt = GetIt.instance;
  HomeBloc? _bloc;

  @override
  void initState() {
    // checkForInitialMessage();
    super.initState();
  }

  @override
  void dispose() {
    _bloc?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    final Size size = MediaQuery.of(context).size;

    //change status bar color
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: themeData.colorScheme.surfaceVariant,
      statusBarIconBrightness: Brightness.dark,
      systemNavigationBarIconBrightness: Brightness.dark,
      systemNavigationBarColor: themeData.colorScheme.surfaceVariant,
    ));

    //handle only portrait Screen
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return Scaffold(
      backgroundColor: themeData.colorScheme.surfaceVariant,
      body: BlocProvider<HomeBloc>(
        create: (context) {
          _bloc = HomeBloc(
            getIt<TicketUserRepository>(),
            getIt<AuthRepository>(),
          );

          _bloc!.add(HomeStarted(""));

          return _bloc!;
        },
        child: SafeArea(
          child: SingleChildScrollView(
            child: BlocBuilder<HomeBloc, HomeState>(
              buildWhen: (p, c) => c is HomeSuccess || c is HomeLoading,
              builder: (context, state) {
                return state is HomeLoading
                    ?
                    //show loading
                    _loading(size, themeData)
                    :
                    //content

                    Padding(
                        padding: padding36H,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            sizedBoxH24,

                            //appbar

                            const HomeAppBar(),
                            sizedBoxH24,
                            Text(
                              "این بخش در آپدیت بعدی فعال خواهد شد!",
                              textAlign: TextAlign.start,
                              overflow: TextOverflow.clip,
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                fontSize: 14,
                                color: Color.fromARGB(179, 255, 74, 74),
                              ),
                            ),
                            sizedBoxH24,

                            Container(
                              margin: EdgeInsets.fromLTRB(0, 16, 0, 0),
                              padding: EdgeInsets.all(0),
                              width: MediaQuery.of(context).size.width,
                              height: 160,
                              decoration: BoxDecoration(
                                color: Color(0xffffffff),
                                shape: BoxShape.rectangle,
                                borderRadius: BorderRadius.zero,
                              ),
                              child: ListView(
                                scrollDirection: Axis.horizontal,
                                padding: EdgeInsets.all(0),
                                shrinkWrap: true,
                                physics: ClampingScrollPhysics(),
                                children: [
                                  Container(
                                      margin: EdgeInsets.fromLTRB(16, 0, 0, 0),
                                      padding: EdgeInsets.all(8),
                                      width: 250,
                                      height: 100,
                                      decoration: BoxDecoration(
                                        color: Color(0xd854256f),
                                        shape: BoxShape.rectangle,
                                        borderRadius:
                                            BorderRadius.circular(16.0),
                                      ),
                                      child: Padding(
                                          padding: EdgeInsets.all(5),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.max,
                                            children: [],
                                          ))),
                                  Container(
                                    margin: EdgeInsets.fromLTRB(16, 0, 0, 0),
                                    padding: EdgeInsets.all(5),
                                    width: 250,
                                    height: 100,
                                    decoration: BoxDecoration(
                                      color: Color(0xff2ec17a),
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(16.0),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.all(5),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.max,
                                        children: [
                                          Text(
                                            "",
                                            textAlign: TextAlign.start,
                                            overflow: TextOverflow.clip,
                                            style: TextStyle(
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                              fontSize: 14,
                                              color: Color(0xffffffff),
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(
                                                0, 16, 0, 0),
                                            child: Text(
                                              "",
                                              textAlign: TextAlign.start,
                                              overflow: TextOverflow.clip,
                                              style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                                fontSize: 18,
                                                color: Color(0xffffffff),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(
                                                0, 40, 0, 0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisSize: MainAxisSize.max,
                                              children: [
                                                Text(
                                                  "",
                                                  textAlign: TextAlign.start,
                                                  overflow: TextOverflow.clip,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w400,
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: 12,
                                                    color: Color(0xffffffff),
                                                  ),
                                                ),
                                                Text(
                                                  "",
                                                  textAlign: TextAlign.start,
                                                  overflow: TextOverflow.clip,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w400,
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: 12,
                                                    color: Color(0xffffffff),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        vertical: 0, horizontal: 16),
                                    padding: EdgeInsets.all(5),
                                    width: 250,
                                    height: 100,
                                    decoration: BoxDecoration(
                                      color: Color(0xe8e27144),
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(16.0),
                                      border: Border.all(
                                          color: Color(0x4d9e9e9e), width: 1),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.all(8),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.max,
                                        children: [
                                          Text(
                                            "",
                                            textAlign: TextAlign.start,
                                            overflow: TextOverflow.clip,
                                            style: TextStyle(
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                              fontSize: 14,
                                              color: Color(0xffffffff),
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(
                                                0, 16, 0, 0),
                                            child: Text(
                                              "",
                                              textAlign: TextAlign.start,
                                              overflow: TextOverflow.clip,
                                              style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                                fontSize: 18,
                                                color: Color(0xffffffff),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(
                                                0, 40, 0, 0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisSize: MainAxisSize.max,
                                              children: [
                                                Text(
                                                  "",
                                                  textAlign: TextAlign.start,
                                                  overflow: TextOverflow.clip,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w400,
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: 12,
                                                    color: Color(0xffffffff),
                                                  ),
                                                ),
                                                Text(
                                                  "",
                                                  textAlign: TextAlign.start,
                                                  overflow: TextOverflow.clip,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w400,
                                                    fontStyle: FontStyle.normal,
                                                    fontSize: 12,
                                                    color: Color(0xffffffff),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            sizedBoxH24,
                            GridView(
                                padding: EdgeInsets.all(16),
                                shrinkWrap: true,
                                scrollDirection: Axis.vertical,
                                physics: ClampingScrollPhysics(),
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  crossAxisSpacing: 16,
                                  mainAxisSpacing: 16,
                                  childAspectRatio: 1,
                                ),
                                children: [
                                  Card(
                                    margin: EdgeInsets.all(0),
                                    color: Color.fromARGB(167, 124, 124, 124),
                                    shadowColor: Color(0xff000000),
                                    elevation: 4,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        Container(
                                          alignment: Alignment.center,
                                          margin: EdgeInsets.all(0),
                                          padding: EdgeInsets.all(12),
                                          decoration: BoxDecoration(
                                            color: Color.fromARGB(
                                                148, 255, 255, 255),
                                            shape: BoxShape.circle,
                                            border: Border.all(
                                                color: Color(0x4d9e9e9e),
                                                width: 1),
                                          ),
                                          child: Icon(
                                            Icons.all_inclusive,
                                            color: Color.fromARGB(
                                                172, 232, 58, 58),
                                            size: 24,
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(0, 12, 0, 8),
                                          child: Text(
                                            "وظایف امروز",
                                            textAlign: TextAlign.center,
                                            maxLines: 1,
                                            overflow: TextOverflow.clip,
                                            style: TextStyle(
                                              fontWeight: FontWeight.w700,
                                              fontStyle: FontStyle.normal,
                                              fontSize: 16,
                                              color: Color.fromARGB(
                                                  255, 255, 255, 255),
                                            ),
                                          ),
                                        ),
                                        Text(
                                          "10 وظیفه",
                                          textAlign: TextAlign.start,
                                          overflow: TextOverflow.clip,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                            fontSize: 14,
                                            color: Color.fromARGB(
                                                255, 214, 214, 214),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Card(
                                    margin: EdgeInsets.all(0),
                                    color: Color.fromARGB(167, 124, 124, 124),
                                    shadowColor: Color(0xff000000),
                                    elevation: 4,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        Container(
                                          alignment: Alignment.center,
                                          margin: EdgeInsets.all(0),
                                          padding: EdgeInsets.all(12),
                                          decoration: BoxDecoration(
                                            color: Color.fromARGB(
                                                148, 255, 255, 255),
                                            shape: BoxShape.circle,
                                          ),
                                          child: Icon(
                                            Icons.access_time,
                                            color: Color.fromARGB(
                                                172, 232, 58, 58),
                                            size: 24,
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(0, 12, 0, 8),
                                          child: Text(
                                            "به تعویق افتاده",
                                            textAlign: TextAlign.center,
                                            maxLines: 1,
                                            overflow: TextOverflow.clip,
                                            style: TextStyle(
                                              fontWeight: FontWeight.w700,
                                              fontStyle: FontStyle.normal,
                                              fontSize: 16,
                                              color: Color.fromARGB(
                                                  255, 223, 223, 223),
                                            ),
                                          ),
                                        ),
                                        Text(
                                          "14 وظیفه",
                                          textAlign: TextAlign.start,
                                          overflow: TextOverflow.clip,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                            fontSize: 14,
                                            color: Color.fromARGB(
                                                255, 214, 214, 214),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Card(
                                    margin: EdgeInsets.all(0),
                                    color: Color.fromARGB(167, 124, 124, 124),
                                    shadowColor: Color(0xff000000),
                                    elevation: 4,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        Container(
                                          alignment: Alignment.center,
                                          margin: EdgeInsets.all(0),
                                          padding: EdgeInsets.all(12),
                                          decoration: BoxDecoration(
                                            color: Color.fromARGB(
                                                148, 255, 255, 255),
                                            shape: BoxShape.circle,
                                          ),
                                          child: Icon(
                                            Icons.alarm,
                                            color: Color.fromARGB(
                                                172, 232, 58, 58),
                                            size: 24,
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(0, 12, 0, 8),
                                          child: Text(
                                            "وظایف مهم",
                                            textAlign: TextAlign.center,
                                            maxLines: 1,
                                            overflow: TextOverflow.clip,
                                            style: TextStyle(
                                              fontWeight: FontWeight.w700,
                                              fontStyle: FontStyle.normal,
                                              fontSize: 16,
                                              color: Color.fromARGB(
                                                  255, 255, 255, 255),
                                            ),
                                          ),
                                        ),
                                        Text(
                                          "2 وظیفه",
                                          textAlign: TextAlign.start,
                                          overflow: TextOverflow.clip,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                            fontSize: 14,
                                            color: Color.fromARGB(
                                                255, 214, 214, 214),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Card(
                                    margin: EdgeInsets.all(0),
                                    color: Color.fromARGB(167, 124, 124, 124),
                                    shadowColor: Color(0xff000000),
                                    elevation: 4,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        Container(
                                          alignment: Alignment.center,
                                          margin: EdgeInsets.all(0),
                                          padding: EdgeInsets.all(12),
                                          decoration: BoxDecoration(
                                            color: Color.fromARGB(
                                                148, 255, 255, 255),
                                            shape: BoxShape.circle,
                                          ),
                                          child: Icon(
                                            Icons.article,
                                            color: Color.fromARGB(
                                                172, 232, 58, 58),
                                            size: 24,
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(0, 12, 0, 8),
                                          child: Text(
                                            "نشان شده",
                                            textAlign: TextAlign.center,
                                            maxLines: 1,
                                            overflow: TextOverflow.clip,
                                            style: TextStyle(
                                              fontWeight: FontWeight.w700,
                                              fontStyle: FontStyle.normal,
                                              fontSize: 16,
                                              color: Color.fromARGB(
                                                  255, 255, 255, 255),
                                            ),
                                          ),
                                        ),
                                        Text(
                                          "6 وظیفه",
                                          textAlign: TextAlign.start,
                                          overflow: TextOverflow.clip,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                            fontSize: 14,
                                            color: Color.fromARGB(
                                                255, 214, 214, 214),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ]),

                            sizedBoxH36,

                            //item create ticket
                            CustomMenuItem(
                              color: Color.fromARGB(167, 124, 124, 124),
                              icon: Image.asset(
                                "assets/image/png/todo_tasks.png",
                                height: 50,
                                width: 50,
                              ),
                              text: _menuItemText(themeData, TodoScreen.item2),
                              onTap: () {
                                Navigator.pushNamed(context, AppCons.route);
                              },
                            ),

                            sizedBoxH20,

                            //item change password
                            CustomMenuItem(
                              color: Color.fromARGB(167, 124, 124, 124),
                              icon: Image.asset(
                                "assets/image/png/todo_clander.png",
                                height: 50,
                                width: 50,
                              ),
                              text: _menuItemText(themeData, TodoScreen.item3),
                              onTap: () {},
                            ),

                            sizedBoxH20,

                            //item create ticket
                            CustomMenuItem(
                              color: Color.fromARGB(167, 124, 124, 124),
                              icon: Image.asset(
                                "assets/image/png/todo_note.png",
                                height: 50,
                                width: 50,
                              ),
                              text: _menuItemText(themeData, TodoScreen.item4),
                              onTap: () {
                                Navigator.pushNamed(context, NotesScreen.route);
                              },
                            ),

                            sizedBoxH20,

                            CustomMenuItem(
                              color: Color.fromARGB(103, 255, 105, 105),
                              icon: Image.asset(
                                "assets/image/png/todo_home.png",
                                height: 50,
                                width: 50,
                              ),
                              text: _menuItemText(themeData, TodoScreen.item6),
                              onTap: () {
                                Navigator.pushNamed(context, HomeScreen.route);
                              },
                            ),

                            const SizedBox(
                              height: HomeRoute.bottomNavHeight -
                                  HomeRoute.bottomNavContainerHeight,
                            ),
                          ],
                        ),
                      );
              },
            ),
          ),
        ),
      ),
    );
  }

  Text _menuItemText(ThemeData themeData, String text) {
    return Text(
      text,
      style: themeData.textTheme.subtitle1!
          .copyWith(color: Color.fromARGB(255, 255, 255, 255)),
    );
  }

  Widget _nameText(ThemeData themeData, String text) {
    return Text(
      text,
      style: themeData.textTheme.headline3,
    );
  }

  Widget _loading(Size size, ThemeData themeData) {
    return SizedBox(
      width: size.width,
      height: size.height,
      child: Center(
        child: showLoading(
          themeData.colorScheme.primary,
        ),
      ),
    );
  }

  showCustomDialog({
    required BuildContext context,
    required String title,
  }) async {
    await showDialog(
      context: context,
      barrierColor: null,
      builder: (context) {
        return BlocProvider.value(
          value: _bloc!,
          child: BlocListener<HomeBloc, HomeState>(
            listenWhen: (p, c) {
              return c is HomeExitSuccess;
            },
            listener: (context, state) {
              if (state is HomeExitSuccess) {
                _backToHome(context);
              }
            },
            //for rtl layout
            child: Directionality(
              textDirection: TextDirection.rtl,
              child: BackdropFilter(
                //for blur behind screen
                filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                child: ExitDialog(
                  title: title,
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  void _backToHome(BuildContext context) {
    Navigator.pushNamedAndRemoveUntil(
        context, LoginScreen.route, (route) => false);
  }
}

part of 'home_bloc.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();

  @override
  List<Object> get props => [];
}

class HomeStarted extends HomeEvent {
  final String token;

  @override
  List<Object> get props => [token];
  const HomeStarted(this.token);
}

class HomeExitButtonClicked extends HomeEvent {
  const HomeExitButtonClicked();

  @override
  List<Object> get props => [];
}

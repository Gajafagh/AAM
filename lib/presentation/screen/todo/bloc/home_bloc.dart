import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:GajAfagh/domain/model/token_container.dart';
import 'package:GajAfagh/domain/repository/auth_repository.dart';
import 'package:GajAfagh/domain/repository/ticket_user_repository.dart';
import 'package:GajAfagh/presentation/screen/auth/login_screen.dart';
import 'package:GajAfagh/presentation/screen/home/home.dart';
import 'package:flutter/material.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

part 'home_event.dart';

part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final TicketUserRepository ticketUserRepository;
  final AuthRepository authRepository;
  late String name;

  HomeBloc(this.ticketUserRepository, this.authRepository)
      : super(HomeLoading()) {
    on<HomeEvent>((event, emit) async {
      if (event is HomeStarted) {
        //start home page
        await emitHomeStartedState(emit, event.token);
      } else if (event is HomeExitButtonClicked) {
        //click exit item
        await emitExitState(emit);
      }
    });
  }

  Future<void> emitHomeStartedState(
      Emitter<HomeState> emit, String toekn) async {
    // UserInfoResponse info;

    //show loading state
    emit(HomeLoading());

    //request for get name
    if (TokenContainer.instance().userId != null) {
      final String userId = TokenContainer.instance().userId!;
      try {
        var ss = ticketUserRepository.userToken(toekn);
      } catch (err) {
        print(err);
        emitExitState(emit);
      }
    }
    name = "به آفاق خوش آمدید";

    //show success state and show name in head page
    emit(HomeSuccess(name));
  }

  Future<void> emitExitState(Emitter<HomeState> emit) async {
    await authRepository.clearData();
    emit(HomeExitSuccess());
  }
}

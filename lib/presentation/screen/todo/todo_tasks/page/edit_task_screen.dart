import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import 'package:GajAfagh/presentation/screen/todo/todo_tasks/scopedmodel/todo_list_model.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/model/task_model.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/component/iconpicker/icon_picker_builder.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/component/colorpicker/color_picker_builder.dart';

class EditTaskScreen extends StatefulWidget {
  final String taskId;
  final String taskName;
  final Color color;
  final IconData icon;

  EditTaskScreen({
    required this.taskId,
    required this.taskName,
    required this.color,
    required this.icon,
  });

  @override
  State<StatefulWidget> createState() {
    return _EditCardScreenState();
  }
}

class _EditCardScreenState extends State<EditTaskScreen> {
  late String taskName;
  late Color taskColor;
  late IconData taskIcon;

  final btnSaveTitle = "تایید";

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    taskName = widget.taskName;
    taskColor = widget.color;
    taskIcon = widget.icon;
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<TodoListModel>(
      builder: (BuildContext context, Widget? child, TodoListModel model) {
        return Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: Text(
              textDirection: TextDirection.rtl,
              'ویرایش دسته بندی',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            centerTitle: true,
            elevation: 0,
            iconTheme: IconThemeData(
              color: Colors.black26,
            ),
            brightness: Brightness.light,
            backgroundColor: Colors.white,
          ),
          body: Container(
            constraints: BoxConstraints.expand(),
            padding: EdgeInsets.symmetric(
              horizontal: 36.0,
              vertical: 36.0,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'جهت نظم در انجام فعالیت های خود، رویدادها را دسته بندی کنید',
                  style: TextStyle(
                    color: Colors.black38,
                    fontWeight: FontWeight.w600,
                    fontSize: 16.0,
                  ),
                ),
                Container(
                  height: 16.0,
                ),
                TextFormField(
                  textDirection: TextDirection.rtl,
                  initialValue: taskName,
                  onChanged: (text) {
                    setState(
                      () => taskName = text,
                    );
                  },
                  cursorColor: taskColor,
                  autofocus: true,
                  decoration: InputDecoration(
                      hintTextDirection: TextDirection.rtl,
                      border: InputBorder.none,
                      hintText: 'نام دسته بندی...',
                      hintStyle: TextStyle(
                        color: Colors.black26,
                      )),
                  style: TextStyle(
                      color: Colors.black54,
                      fontWeight: FontWeight.w500,
                      fontSize: 28.0),
                ),
                Container(
                  height: 26.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconPickerBuilder(
                      iconData: taskIcon,
                      highlightColor: taskColor,
                      action: (newIcon) => setState(
                        () => taskIcon = newIcon,
                      ),
                    ),
                    Container(
                      width: 22.0,
                    ),
                    ColorPickerBuilder(
                      color: taskColor,
                      onColorChanged: (newColor) => setState(
                        () => taskColor = newColor,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerFloat,
          floatingActionButton: Builder(
            builder: (BuildContext context) {
              return Container(
                  margin: EdgeInsets.only(bottom: 40),
                  child: FloatingActionButton.extended(
                    heroTag: 'fab_new_card',
                    icon: Icon(Icons.save),
                    backgroundColor: taskColor,
                    label: Text(btnSaveTitle),
                    onPressed: () {
                      if (taskName.isEmpty) {
                        final snackBar = SnackBar(
                          content: Text('اطلاعات ورودی صحیح نیست'),
                          backgroundColor: taskColor,
                        );
                        // Scaffold.of(context).showSnackBar(snackBar);
                        // _scaffoldKey.currentState.showSnackBar(snackBar);
                      } else {
                        model.updateTask(
                          Task(
                            taskName,
                            codePoint: taskIcon.codePoint,
                            color: taskColor.value,
                            id: widget.taskId,
                          ),
                        );
                        Navigator.pop(context);
                      }
                    },
                  ));
            },
          ),
        );
      },
    );
  }
}

// Reason for wraping fab with builder (to get scafold context)
// https://stackoverflow.com/a/52123080/4934757

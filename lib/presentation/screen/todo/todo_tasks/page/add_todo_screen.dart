import 'package:GajAfagh/gen/fonts.gen.dart';
import 'package:GajAfagh/util/extension.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:persian_datetime_picker/persian_datetime_picker.dart';
import 'package:scoped_model/scoped_model.dart';

import 'package:GajAfagh/presentation/screen/todo/todo_tasks/scopedmodel/todo_list_model.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/model/todo_model.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/utils/color_utils.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/component/todo_badge.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/model/hero_id_model.dart';

class AddTodoScreen extends StatefulWidget {
  final String taskId;
  final HeroId heroIds;

  AddTodoScreen({
    required this.taskId,
    required this.heroIds,
  });

  @override
  State<StatefulWidget> createState() {
    return _AddTodoScreenState();
  }
}

class _AddTodoScreenState extends State<AddTodoScreen> {
  late String newTask;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String selectedDate = Jalali.now().formatFullDate();
  DateTime selectedDateEn = DateTime.now();
  String selectedTime = "${TimeOfDay.now().hour}:${TimeOfDay.now().minute}";
  String selectedEndTime =
      "${TimeOfDay.now().hour}:${TimeOfDay.now().minute + 2}";
  @override
  void initState() {
    super.initState();
    newTask = '';
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<TodoListModel>(
      builder: (BuildContext context, Widget? child, TodoListModel model) {
        if (model.tasks.isEmpty) {
          // Loading
          return Container(
            color: Colors.white,
          );
        }

        var _task = model.tasks.firstWhere((it) => it.id == widget.taskId);
        var _color = ColorUtils.getColorFrom(id: _task.color);

        return Scaffold(
          key: _scaffoldKey,
          backgroundColor: Color.fromARGB(255, 255, 255, 255),
          appBar: AppBar(
            title: Text(
              'رویداد جدید',
              style: TextStyle(color: Colors.black),
            ),
            centerTitle: true,
            elevation: 0,
            iconTheme: IconThemeData(color: Colors.black26),
            brightness: Brightness.light,
            backgroundColor: Colors.white,
          ),
          body: Container(
            constraints: BoxConstraints.expand(),
            padding: EdgeInsets.symmetric(horizontal: 36.0, vertical: 36.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  textDirection: TextDirection.rtl,
                  'چه رویداد جدیدی برای تنظیم دارید؟',
                  style: TextStyle(
                    color: Colors.black38,
                    fontWeight: FontWeight.w600,
                    fontSize: 16.0,
                  ),
                ),
                Container(
                  height: 16.0,
                ),
                TextField(
                  textDirection: TextDirection.rtl,
                  onChanged: (text) {
                    setState(() => newTask = text);
                  },
                  cursorColor: _color,
                  autofocus: true,
                  decoration: const InputDecoration(
                      hintTextDirection: TextDirection.rtl,
                      border: InputBorder.none,
                      hintText: 'نام رویداد ...',
                      hintStyle: TextStyle(
                        color: Colors.black26,
                      )),
                  style: TextStyle(
                      color: Colors.black54,
                      fontWeight: FontWeight.w500,
                      fontSize: 28.0),
                ),
                Container(
                  height: 26.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Hero(
                      child: Text(
                        _task.name,
                        style: TextStyle(
                          color: Colors.black38,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      tag: "not_using_right_now", //widget.heroIds.titleId,
                    ),
                    Container(
                      width: 16.0,
                    ),
                    TodoBadge(
                      codePoint: _task.codePoint,
                      color: _color,
                      id: widget.heroIds.codePointId,
                      size: 20.0,
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        margin: EdgeInsets.only(top: 20),
                        alignment: Alignment.center,
                        child: InkWell(
                            borderRadius: BorderRadius.circular(50.0),
                            onTap: () {},
                            child: FloatingActionButton.extended(
                                heroTag: 'fab_new_date',
                                icon: Icon(Icons.date_range),
                                backgroundColor: _color.withOpacity(0.4),
                                label: Text('انتخاب تاریخ'),
                                onPressed: () async {
                                  Jalali? picked = await showPersianDatePicker(
                                      context: context,
                                      initialDate: Jalali.now(),
                                      firstDate: Jalali(1385, 8),
                                      lastDate: Jalali(1450, 9),
                                      initialDatePickerMode:
                                          PDatePickerMode.year,
                                      builder: (condex, child) {
                                        return Theme(
                                          data: ThemeData(
                                            fontFamily: FontFamily.kalameh,
                                            dialogTheme: const DialogTheme(
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8)),
                                              ),
                                            ),
                                          ),
                                          child: child!,
                                        );
                                      });
                                  if (picked != null &&
                                      picked != selectedDate) {
                                    setState(() {
                                      selectedDate = picked.formatFullDate();
                                      selectedDateEn = picked.toDateTime();
                                    });
                                  }
                                }
                                // showDialog(
                                //   context: context,
                                //   builder: (BuildContext context)  {
                                //     return AlertDialog(
                                //       title: Text(
                                //         'تاریخ و زمان مورد نظر خود',
                                //         textAlign: TextAlign.center,
                                //       ),
                                //       content: SingleChildScrollView(
                                //         child:
                                //       ),
                                //     );
                                //   },
                                // );

                                ))),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        margin: EdgeInsets.only(top: 20, right: 10),
                        alignment: Alignment.center,
                        child: InkWell(
                            borderRadius: BorderRadius.circular(50.0),
                            onTap: () {},
                            child: FloatingActionButton.extended(
                              heroTag: 'fab_new_end_time',
                              icon: Icon(Icons.add_alarm),
                              backgroundColor: _color.withOpacity(0.4),
                              label: Text('زمان پایان'),
                              onPressed: () async {
                                TimeOfDay? picked = await showTimePicker(
                                    confirmText: "تایید",
                                    cancelText: "لغو",
                                    helpText: "زمان اعلام هشدار را انتخاب کنید",
                                    errorInvalidText:
                                        "یک زمان درست را انتخاب کنید",
                                    hourLabelText: "ساعت",
                                    minuteLabelText: "دقیقه",
                                    context: context,
                                    initialTime: TimeOfDay.now().replacing(
                                        minute: TimeOfDay.now().minute + 2),
                                    builder: (condex, child) {
                                      return Theme(
                                        data: ThemeData(
                                          fontFamily: FontFamily.kalameh,
                                          dialogTheme: const DialogTheme(
                                            alignment: Alignment.center,
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(8)),
                                            ),
                                          ),
                                        ),
                                        child: child!,
                                      );
                                    });

                                // showDialog(
                                //   context: context,
                                //   builder: (BuildContext context)  {
                                //     return AlertDialog(
                                //       title: Text(
                                //         'تاریخ و زمان مورد نظر خود',
                                //         textAlign: TextAlign.center,
                                //       ),
                                //       content: SingleChildScrollView(
                                //         child:
                                //       ),
                                //     );
                                //   },
                                // );
                                if (picked!.hour >=
                                        int.parse(selectedTime.split(":")[0]) &&
                                    picked.minute >=
                                        int.parse(selectedTime.split(":")[1])) {
                                  if (selectedDateEn != DateTime.now()) {
                                    if ("${picked.hour}:${picked.minute}" !=
                                        selectedEndTime) {
                                      setState(() {
                                        selectedEndTime =
                                            "${picked.hour}:${picked.minute}";
                                      });
                                    }
                                  }
                                } else {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      padding:
                                          EdgeInsets.only(bottom: 30, top: 30),
                                      content: Text(
                                        "زمان پایان باید از زمان شروع بزرگتر باشد!",
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  );
                                }
                              },
                            ))),
                    Container(
                        margin: EdgeInsets.only(top: 20, right: 10),
                        alignment: Alignment.center,
                        child: InkWell(
                            borderRadius: BorderRadius.circular(50.0),
                            onTap: () {},
                            child: FloatingActionButton.extended(
                              heroTag: 'fab_new_time',
                              icon: Icon(Icons.add_alarm),
                              backgroundColor: _color.withOpacity(0.4),
                              label: Text('زمان شروع'),
                              onPressed: () async {
                                TimeOfDay? picked = await showTimePicker(
                                    confirmText: "تایید",
                                    cancelText: "لغو",
                                    helpText: "زمان اعلام هشدار را انتخاب کنید",
                                    errorInvalidText:
                                        "یک زمان درست را انتخاب کنید",
                                    hourLabelText: "ساعت",
                                    minuteLabelText: "دقیقه",
                                    context: context,
                                    initialTime: TimeOfDay.now(),
                                    builder: (condex, child) {
                                      return Theme(
                                        data: ThemeData(
                                          fontFamily: FontFamily.kalameh,
                                          dialogTheme: const DialogTheme(
                                            alignment: Alignment.center,
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(8)),
                                            ),
                                          ),
                                        ),
                                        child: child!,
                                      );
                                    });

                                // showDialog(
                                //   context: context,
                                //   builder: (BuildContext context)  {
                                //     return AlertDialog(
                                //       title: Text(
                                //         'تاریخ و زمان مورد نظر خود',
                                //         textAlign: TextAlign.center,
                                //       ),
                                //       content: SingleChildScrollView(
                                //         child:
                                //       ),
                                //     );
                                //   },
                                // );
                                if ("${picked?.hour}:${picked?.minute}" !=
                                    selectedTime) {
                                  setState(() {
                                    selectedTime =
                                        "${picked?.hour}:${picked?.minute}";
                                  });
                                }
                              },
                            ))),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        margin: EdgeInsets.only(top: 30),
                        alignment: Alignment.center,
                        child: Text("زمان شروع رویداد: $selectedTime ")),
                  ],
                ),
                Container(
                    margin: EdgeInsets.only(top: 10),
                    alignment: Alignment.center,
                    child: Text("زمان پایان رویداد: $selectedEndTime ")),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        margin: EdgeInsets.only(top: 10),
                        alignment: Alignment.center,
                        child: Text("تاریخ رویداد: $selectedDate")),
                  ],
                )
              ],
            ),
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerFloat,
          floatingActionButton: Builder(
            builder: (BuildContext context) {
              return Container(
                  margin: EdgeInsets.only(bottom: 40),
                  child: FloatingActionButton.extended(
                    heroTag: 'fab_new_task',
                    icon: Icon(Icons.add),
                    backgroundColor: _color,
                    label: Text('ساخت رویداد'),
                    onPressed: () async {
                      if (newTask.isEmpty) {
                        final snackBar = SnackBar(
                          content: Text('اطلاعات ورودی صحیح نیست'),
                          backgroundColor: _color,
                        );
                        // Scaffold.of(context).showSnackBar(snackBar);
                        // _scaffoldKey.currentState.showSnackBar(snackBar);
                      } else {
                        String localTimeZone = await AwesomeNotifications()
                            .getLocalTimeZoneIdentifier();

                        var shd;
                        if (selectedDateEn.day == DateTime.now().day &&
                            selectedDateEn.month == DateTime.now().month &&
                            selectedDateEn.year == DateTime.now().year) {
                          shd = ((int.parse(selectedTime.split(":")[0]) -
                                      DateTime.now().hour) *
                                  3600) +
                              ((int.parse(selectedTime.split(":")[1]) -
                                      DateTime.now().minute) *
                                  60);
                        }
                        print(shd);
                        await AwesomeNotifications().createNotification(
                            content: NotificationContent(
                              id: 1,
                              channelKey: 'basic_channel',
                              title: 'یادت نره این رویداد رو شروع کنی!',
                              body: 'زمان انجام رویداد $newTask رسیده!',
                              icon: null,
                              wakeUpScreen: true,
                              notificationLayout: NotificationLayout.BigPicture,
                              bigPicture: 'asset://assets/image/png/icon.png',
                              payload: {'uuid': 'uuid-test'},
                            ),
                            schedule: NotificationInterval(
                                interval: shd,
                                timeZone: localTimeZone,
                                repeats: false));
                        model.addTodo(Todo(
                          newTask,
                          parent: _task.id,
                        ));
                        Navigator.pop(context);
                      }
                    },
                  ));
            },
          ),
        );
      },
    );
  }
}

// Reason for wraping fab with builder (to get scafold context)
// https://stackoverflow.com/a/52123080/4934757

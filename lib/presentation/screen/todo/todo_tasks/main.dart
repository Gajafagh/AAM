import 'package:GajAfagh/presentation/screen/home/home_route.dart';
import 'package:GajAfagh/presentation/screen/settings/setting.dart';
import 'package:GajAfagh/presentation/theme.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shamsi_date/shamsi_date.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/scopedmodel/todo_list_model.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/gradient_background.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/task_progress_indicator.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/page/add_task_screen.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/model/hero_id_model.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/model/task_model.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/route/scale_route.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/utils/color_utils.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/utils/datetime_utils.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/page/detail_screen.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/component/todo_badge.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/model/data/choice_card.dart';
import 'package:percent_indicator/percent_indicator.dart';

class AppCons extends StatelessWidget {
  static const String route = '/tasks';

  @override
  Widget build(BuildContext context) {
    var app = HomeRoute(
        child: MaterialApp(
      title: 'barname rizi',
      debugShowCheckedModeBanner: false,
      theme: AppThemeConfig.light().getTheme(),
      home: MyHomePage(title: ''),
    ));

    return ScopedModel<TodoListModel>(
      model: TodoListModel(),
      child: app,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  HeroId _generateHeroIds(Task task) {
    return HeroId(
      codePointId: 'code_point_id_${task.id}',
      progressId: 'progress_id_${task.id}',
      titleId: 'title_id_${task.id}',
      remainingTaskId: 'remaining_task_id_${task.id}',
    );
  }

  String format1(Date d) {
    final f = d.formatter;

    return '${f.wN} ${f.d} ${f.mN} ${f.yy}';
  }

  String currentDay(BuildContext context) {
    DateTime dateTime = DateTime.now();
    var a = Jalali.fromDateTime(dateTime);
    return format1(a);
  }

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;
  final GlobalKey _backdropKey = GlobalKey(debugLabel: 'Backdrop');
  late PageController _pageController;
  int _currentPageIndex = 0;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300),
    );
    _animation = Tween<double>(begin: 0.0, end: 1.0).animate(_controller);
    _pageController = PageController(initialPage: 0, viewportFraction: 0.8);
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<TodoListModel>(
        builder: (BuildContext context, Widget? child, TodoListModel model) {
      var _isLoading = model.isLoading;
      var _tasks = model.tasks;
      var _todos = model.todos;
      var backgroundColor = _tasks.isEmpty || _tasks.length == _currentPageIndex
          ? Colors.blueGrey
          : ColorUtils.getColorFrom(id: _tasks[_currentPageIndex].color);
      if (!_isLoading) {
        // move the animation value towards upperbound only when loading is complete
        _controller.forward();
      }
      return GradientBackground(
        color: backgroundColor,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            title: Text(widget.title),
            centerTitle: true,
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            actions: [
              PopupMenuButton<Choice>(
                onSelected: (choice) {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return const Directionality(
                      textDirection: TextDirection.rtl,
                      child: SettingScreen(),
                    );
                  }));
                },
                itemBuilder: (BuildContext context) {
                  return choices.map((Choice choice) {
                    return PopupMenuItem<Choice>(
                      value: choice,
                      child: Text(choice.title),
                    );
                  }).toList();
                },
              ),
            ],
          ),
          body: _isLoading
              ? Center(
                  child: CircularProgressIndicator(
                    strokeWidth: 1.0,
                    valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                  ),
                )
              : FadeTransition(
                  opacity: _animation,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 0.0, right: 15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            // ShadowImage(),
                            Container(
                              alignment: Alignment.center,
                              // margin: EdgeInsets.only(top: 22.0),
                              child: Text(
                                '${widget.currentDay(context)}',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline2
                                    ?.copyWith(color: Colors.white),
                              ),
                            ),
                            Container(height: 12.0),
                            Container(
                              alignment: Alignment.center,
                              // margin: EdgeInsets.only(top: 22.0),
                              child: Text(
                                'شما ${_todos.where((todo) => todo.isCompleted == 0).length} رویداد باز دارید',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline5
                                    ?.copyWith(
                                        color: Colors.white.withOpacity(0.7)),
                              ),
                            ),
                            Container(
                              height: 16.0,
                            )
                            // Container(
                            //   margin: EdgeInsets.only(top: 42.0),
                            //   child: Text(
                            //     'TODAY : FEBURARY 13, 2019',
                            //     style: Theme.of(context)
                            //         .textTheme
                            //         .subtitle
                            //         .copyWith(color: Colors.white.withOpacity(0.8)),
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                      Expanded(
                        key: _backdropKey,
                        flex: 1,
                        child: NotificationListener<ScrollNotification>(
                          onNotification: (notification) {
                            if (notification is ScrollEndNotification) {
                              print(
                                  "ScrollNotification = ${_pageController.page}");
                              var currentPage =
                                  _pageController.page?.round().toInt() ?? 0;
                              if (_currentPageIndex != currentPage) {
                                setState(() => _currentPageIndex = currentPage);
                              }
                            }
                            return true;
                          },
                          child: PageView.builder(
                            controller: _pageController,
                            itemBuilder: (BuildContext context, int index) {
                              if (index == _tasks.length) {
                                return AddPageCard(
                                  color: Colors.blueGrey,
                                );
                              } else {
                                return TaskCard(
                                  backdropKey: _backdropKey,
                                  color: ColorUtils.getColorFrom(
                                      id: _tasks[index].color),
                                  getHeroIds: widget._generateHeroIds,
                                  getTaskCompletionPercent:
                                      model.getTaskCompletionPercent,
                                  getTotalTodos: model.getTotalTodosFrom,
                                  task: _tasks[index],
                                );
                              }
                            },
                            itemCount: _tasks.length + 1,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 32.0),
                      ),
                    ],
                  ),
                ),
        ),
      );
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}

class AddPageCard extends StatelessWidget {
  final Color color;

  const AddPageCard({Key? key, this.color = Colors.black}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),
      elevation: 4.0,
      margin: EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
      child: Material(
        borderRadius: BorderRadius.circular(16.0),
        color: Colors.white,
        child: InkWell(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => AddTaskScreen()));
          },
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.add,
                  size: 52.0,
                  color: color,
                ),
                Container(
                  height: 8.0,
                ),
                Container(
                  alignment: Alignment.center,
                  // margin: EdgeInsets.only(top: 22.0),
                  child: Text(
                    'دسته بندی جدید',
                    style: TextStyle(color: color),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

typedef TaskGetter<T, V> = V Function(T value);

class TaskCard extends StatelessWidget {
  final GlobalKey backdropKey;
  final Task task;
  final Color color;

  final TaskGetter<Task, int> getTotalTodos;
  final TaskGetter<Task, HeroId> getHeroIds;
  final TaskGetter<Task, int> getTaskCompletionPercent;

  TaskCard({
    required this.backdropKey,
    required this.color,
    required this.task,
    required this.getTotalTodos,
    required this.getHeroIds,
    required this.getTaskCompletionPercent,
  });

  @override
  Widget build(BuildContext context) {
    var heroIds = getHeroIds(task);
    return GestureDetector(
      onTap: () {
        final RenderBox? renderBox =
            backdropKey.currentContext?.findRenderObject() as RenderBox;
        var backDropHeight = renderBox?.size.height ?? 0;
        var bottomOffset = 60.0;
        var horizontalOffset = 52.0;
        var topOffset = MediaQuery.of(context).size.height - backDropHeight;

        var rect = RelativeRect.fromLTRB(
            horizontalOffset, topOffset, horizontalOffset, bottomOffset);

        Navigator.of(context).push(
          ScaleRoute(
            rect: rect,
            widget: DetailScreen(
              taskId: task.id,
              heroIds: heroIds,
            ),
          ),
        );
        // Navigator.push(
        //   context,
        //   ScaleRoute(
        //     rect: rect,
        //     widget: DetailScreen(
        //       taskId: task.id,
        //       heroIds: heroIds,
        //     ),
        //   ),
        // MaterialPageRoute(
        //   builder: (context) => DetailScreen(
        //         taskId: task.id,
        //         heroIds: heroIds,
        //       ),
        // ),
        // );
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        elevation: 4.0,
        margin: EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TodoBadge(
                id: heroIds.codePointId,
                codePoint: task.codePoint,
                color: ColorUtils.getColorFrom(
                  id: task.color,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 30),
                alignment: Alignment.center,
                child: CircularPercentIndicator(
                  radius: 60.0,
                  lineWidth: 5.0,
                  percent:
                      ((100 - getTaskCompletionPercent(task)) / 100).toDouble(),
                  center:
                      new Text("%${((100 - getTaskCompletionPercent(task)))}"),
                  progressColor: color,
                ),
              ),
              Spacer(
                flex: 2,
              ),
              Container(
                alignment: Alignment.center,
                // margin: EdgeInsets.only(top: 22.0),
                child: Text(
                  "نمودار درصد رویداد های باقی مانده",
                  style: Theme.of(context)
                      .textTheme
                      .subtitle2
                      ?.copyWith(color: Colors.grey[500]),
                ),
              ),
              Spacer(
                flex: 5,
              ),
              Container(
                alignment: Alignment.center,
                child: Hero(
                  tag: heroIds.titleId,
                  child: Text(task.name,
                      style: Theme.of(context)
                          .textTheme
                          .headline3
                          ?.copyWith(color: Color.fromARGB(255, 0, 0, 0))),
                ),
              ),
              Spacer(
                flex: 5,
              ),
              Container(
                margin: EdgeInsets.only(bottom: 4.0),
                child: Hero(
                  tag: heroIds.remainingTaskId,
                  child: Container(
                    alignment: Alignment.center,
                    // margin: EdgeInsets.only(top: 22.0),
                    child: Text(
                      "تعداد رویداد ها: ${getTotalTodos(task)}",
                      style: Theme.of(context)
                          .textTheme
                          .subtitle2
                          ?.copyWith(color: Colors.grey[500]),
                    ),
                  ),
                ),
              ),
              Spacer(),
              Hero(
                tag: heroIds.progressId,
                child: TaskProgressIndicator(
                  color: color,
                  progress: getTaskCompletionPercent(task),
                ),
              ),
              Container(
                alignment: Alignment.center,
                // margin: EdgeInsets.only(top: 22.0),
                child: Text(
                  "نمودار درصد انجام فعالیت ها",
                  style: Theme.of(context)
                      .textTheme
                      .subtitle2
                      ?.copyWith(color: Colors.grey[500]),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

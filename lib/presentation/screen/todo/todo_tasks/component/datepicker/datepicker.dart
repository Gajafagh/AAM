import 'package:flutter/material.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/component/todo_badge.dart';
import 'package:persian_datetime_picker/persian_datetime_picker.dart';

class DateBuilder extends StatelessWidget {
  const DateBuilder();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(50.0),
      onTap: () async {
        await showPersianDateRangePicker(
          context: context,
          initialEntryMode: PDatePickerEntryMode.input,
          initialDateRange: JalaliRange(
            start: Jalali(1400, 1, 2),
            end: Jalali(1400, 1, 10),
          ),
          firstDate: Jalali(1385, 8),
          lastDate: Jalali(1450, 9),
        );
        // showDialog(
        //   context: context,
        //   builder: (BuildContext context)  {
        //     return AlertDialog(
        //       title: Text(
        //         'تاریخ و زمان مورد نظر خود',
        //         textAlign: TextAlign.center,
        //       ),
        //       content: SingleChildScrollView(
        //         child:
        //       ),
        //     );
        //   },
        // );
      },
    );
  }
}

import 'package:flutter/material.dart';

class NoteForm extends StatelessWidget {
  final String? title;
  final String? description;
  final String? category;
  final ValueChanged<String> onChangedTitle;
  final ValueChanged<String> onChangedDescription;
  final ValueChanged<String> onChangedCtegory;

  const NoteForm({
    Key? key,
    this.title = '',
    this.description = '',
    this.category = '',
    required this.onChangedCtegory,
    required this.onChangedTitle,
    required this.onChangedDescription,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              buildTitle(),
              const Divider(
                color: Colors.amber,
              ),
              const SizedBox(height: 8),
              buildDescription(),
              const SizedBox(height: 16),
              buildCategory(),
            ],
          ),
        ),
      );

  Widget buildTitle() => TextFormField(
        maxLines: 1,
        initialValue: title,
        style: const TextStyle(
          color: Colors.black87,
          fontWeight: FontWeight.bold,
          fontSize: 24,
        ),
        decoration: const InputDecoration(
          border: InputBorder.none,
          hintText: 'موضوع یادداشت',
          hintStyle: TextStyle(color: Color.fromARGB(221, 211, 108, 108)),
        ),
        validator: (title) => title != null && title.isEmpty
            ? 'موضوع نمیتواند خالی باشد!'
            : null, //validator for checking null title
        onChanged: onChangedTitle,
      );

  Widget buildDescription() => TextFormField(
        maxLines: 5,
        initialValue: description,
        style: const TextStyle(color: Colors.black87, fontSize: 18),
        decoration: const InputDecoration(
          hintText: 'متن یادداشت ...',
          hintStyle: TextStyle(color: Color.fromARGB(221, 107, 107, 107)),
        ),
        validator: (description) => description != null &&
                description.isEmpty //validator for checking null description
            ? 'یادداشت نمیتواند حالی باشد!'
            : null,
        onChanged: onChangedDescription,
      );
  Widget buildCategory() => TextFormField(
        maxLines: 1,
        initialValue: category,
        style: const TextStyle(color: Colors.black87, fontSize: 18),
        decoration: const InputDecoration(
          hintText: 'نام دسته بندی ( پیشفرض: همه )',
          hintStyle: TextStyle(color: Color.fromARGB(221, 107, 107, 107)),
        ),
        validator: (category) => null,
        onChanged: onChangedCtegory,
      );
}

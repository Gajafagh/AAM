class CategoryModel {
  late String categoryName;
}

List<CategoryModel> getCategories() {
  List<CategoryModel> categories = [];

  CategoryModel categoryModel = CategoryModel();

  categoryModel = CategoryModel();
  categoryModel.categoryName = "همه";
  categories.add(categoryModel);

  categoryModel = CategoryModel();
  categoryModel.categoryName = "دسته بندی ۱";
  categories.add(categoryModel);

  categoryModel = CategoryModel();
  categoryModel.categoryName = "دسته بندی ۲";
  categories.add(categoryModel);

  categoryModel = CategoryModel();
  categoryModel.categoryName = "دسته بندی ۳";
  categories.add(categoryModel);

  categoryModel = CategoryModel();
  categoryModel.categoryName = "دسته بندی ۴";
  categories.add(categoryModel);

  return categories;
}

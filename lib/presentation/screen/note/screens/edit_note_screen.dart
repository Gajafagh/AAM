import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:GajAfagh/presentation/screen/note/model/note_model.dart';
import 'package:GajAfagh/presentation/screen/note/sqflite_database/db.dart';
import 'package:GajAfagh/presentation/screen/note/widgets/note_form.dart';

class EditNoteScreen extends StatefulWidget {
  static const String route = '/note-edit';
  const EditNoteScreen({Key? key, this.note}) : super(key: key);
  final Note? note;

  @override
  State<EditNoteScreen> createState() => _EditNoteScreenState();
}

class _EditNoteScreenState extends State<EditNoteScreen> {
  final _formKey = GlobalKey<FormState>();
  late String title;
  late String description;
  late String category;
  late IconData generalIcon;
  @override
  void initState() {
    super.initState();
    title = widget.note?.title ?? ''; // by default empty
    description = widget.note?.description ?? '';
    category = widget.note?.category ?? ''; // by default empty
  }

  @override
  Widget build(BuildContext context) => Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 1,
          leading: IconButton(
            icon: const Icon(FontAwesomeIcons.backward),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          actions: [buildButton()]),
      body: Form(
        key: _formKey,
        child: NoteForm(
          title: title,
          description: description,
          category: category,
          onChangedTitle: (title) => setState(() => this.title = title),
          onChangedDescription: (description) =>
              setState(() => this.description = description),
          onChangedCtegory: (category) =>
              setState(() => this.category = category),
        ),
      ),
      bottomNavigationBar: Container(
          padding: EdgeInsets.only(bottom: 60),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(
                tooltip: "افزودن فایل صوتی",
                icon: const Icon(
                  FontAwesomeIcons.music,
                  color: Colors.red,
                ),
                onPressed: () {},
              ),
              IconButton(
                tooltip: "افزودن عکس",
                icon: const Icon(
                  FontAwesomeIcons.squarePollHorizontal,
                  color: Colors.green,
                ),
                onPressed: () {},
              ),
              IconButton(
                tooltip: "افزودن موضوع",
                icon: Icon(
                  FontAwesomeIcons.bell,
                  color: Colors.blue.shade900,
                ),
                onPressed: () {},
              ),
              IconButton(
                tooltip: "افزودن یادآوری",
                icon: const Icon(
                  FontAwesomeIcons.circleCheck,
                  color: Colors.yellow,
                ),
                onPressed: () {},
              ),
            ],
          )));

  Widget buildButton() {
    final isFormValid =
        title.isNotEmpty && description.isNotEmpty; // check is form valid

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          onPrimary: Colors.white,
          primary: isFormValid ? Colors.teal : Colors.grey.shade700,
        ),
        onPressed: addOrUpdateNote,
        child: const Icon(
          FontAwesomeIcons.check,
        ),
      ),
    );
  }

//addOrUpdateNote
  void addOrUpdateNote() async {
    final isValid = _formKey.currentState!.validate();
    if (isValid) {
      final isUpdating = widget.note != null;
      if (isUpdating) {
        await updateNote();
      } else {
        await addNote();
      }
      Navigator.of(context).pop();
    }
  }

//updateNote
  Future updateNote() async {
    final note = widget.note!.copy(
      title: title,
      description: description,
      createdTime: DateTime.now(),
    );
    await NotesDatabase.instance.update(note);
  }

//addNote
  Future addNote() async {
    final note = Note(
      title: title,
      description: description,
      category: category,
      createdTime: DateTime.now(),
    );
    await NotesDatabase.instance.create(note);
  }
}

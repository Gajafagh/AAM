import 'package:flutter/material.dart';
import 'package:GajAfagh/gen/assets.gen.dart';
import 'package:GajAfagh/presentation/color.dart';
import 'package:GajAfagh/presentation/component/dimension.dart';
import 'package:GajAfagh/presentation/component/widget/gradiant_button.dart';
import 'package:GajAfagh/presentation/component/widget/gradiant_text.dart';

class AppBarAuth extends StatelessWidget {
  final String text;
  final Gradient gradient;
  final Function() onTap;

  const AppBarAuth({
    Key? key,
    required this.text,
    required this.gradient,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);
    return Container(
      padding: appbarAuthPadding,
      decoration: BoxDecoration(
        borderRadius: circular32,
        color: themeData.colorScheme.surface,
      ),
      child: Row(
        children: [
          GradientText(
            'نرم افزار اتوماسیون',
            gradient: LightColorPalette.defaultTextGradiant,
            style: themeData.textTheme.headline5,
          ),
          //logo
          const Spacer(),
          Image.asset(
            "assets/image/png/icon.png",
            scale: 6,
          ),

          sizedBoxW8,

          //appbar title

          //appbar action
        ],
      ),
    );
  }
}

class AppBarAuth1 extends StatelessWidget {
  final String text;
  final Gradient gradient;
  final Function() onTap;

  const AppBarAuth1({
    Key? key,
    required this.text,
    required this.gradient,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);
    return Container(
      padding: appbarAuthPadding,
      decoration: BoxDecoration(
        borderRadius: circular32,
        color: themeData.colorScheme.surface,
      ),
    );
  }
}

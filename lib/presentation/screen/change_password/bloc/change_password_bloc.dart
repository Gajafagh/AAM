import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:GajAfagh/data/remote/util/server_error.dart';
import 'package:GajAfagh/domain/repository/auth_repository.dart';

part 'change_password_event.dart';

part 'change_password_state.dart';

class ChangePasswordBloc
    extends Bloc<ChangePasswordEvent, ChangePasswordState> {
  final AuthRepository authRepository;

  ChangePasswordBloc(this.authRepository) : super(ChangePasswordInitial()) {
    on<ChangePasswordEvent>((event, emit) async {
      if (event is ChangePasswordClickSubmitButton) {
        await submitButtonClickedHandler(
          emit: emit,
          currentPass: event.currentPass,
          newPass: event.newPass,
          confirmNewPass: event.confirmNewPass,
        );
      }
    });
  }

  Future<void> submitButtonClickedHandler({
    required Emitter<ChangePasswordState> emit,
    required String currentPass,
    required String newPass,
    required String confirmNewPass,
  }) async {
    try {
      emit(ChangePasswordLoading());

      //check same newPassword
      if (!checkConfirmNewPassword(newPass, confirmNewPass)) {
        emit(
          ChangePasswordError(
            CustomError(message: 'تکرار رمزعبور با رمزعبور یکسان نیست'),
          ),
        );
        return;
      }

      final result = await authRepository
          .changePassword(currentPass, newPass)
          .timeout(const Duration(seconds: 8));

      //error response server
      if (!result) {
        emit(
          ChangePasswordError(
            CustomError(message: 'کلمه عبور فعلی اشتباه وارد شده است!'),
          ),
        );
        return;
      }

      //success state
      emit(ChangePasswordSuccess());
    } catch (ex) {
      emit(
        ChangePasswordError(
          ex is CustomError
              ? ex
              : CustomError(message: 'کلمه عبور فعلی اشتباه وارد شده است!'),
        ),
      );
    }
  }

  bool checkConfirmNewPassword(String newPass, String confirmPass) {
    return newPass == confirmPass;
  }
}

import 'package:GajAfagh/presentation/screen/note/screens/edit_note_screen.dart';
import 'package:GajAfagh/presentation/screen/note/screens/notes_detail_screen.dart';
import 'package:GajAfagh/presentation/screen/settings/setting.dart';
import 'package:GajAfagh/presentation/screen/ticket/add_ticket/answer_ticket.dart';
import 'package:GajAfagh/presentation/screen/ticket/add_ticket/reply_ticket.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_clander/clander.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/main.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_tasks/page/add_task_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:GajAfagh/domain/repository/auth_repository.dart';
import 'package:GajAfagh/presentation/screen/auth/bloc/auth_bloc.dart';
import 'package:GajAfagh/presentation/screen/auth/forget_password_screen.dart';
import 'package:GajAfagh/presentation/screen/auth/login_screen.dart';
import 'package:GajAfagh/presentation/screen/auth/otp_screen.dart';
import 'package:GajAfagh/presentation/screen/auth/register_screen.dart';
import 'package:GajAfagh/presentation/screen/change_password/change_password.dart';
import 'package:GajAfagh/presentation/screen/home/home.dart';
import 'package:GajAfagh/presentation/screen/splash.dart';
import 'package:GajAfagh/presentation/screen/ticket/add_ticket/add_ticket_screen.dart';
import 'package:GajAfagh/presentation/screen/ticket/list_ticket/list_ticket_screen.dart';
import 'package:GajAfagh/presentation/screen/todo/todo_home.dart';
import 'package:GajAfagh/presentation/screen/note/screens/notes_screen.dart';

class AppRoute {
  final getIt = GetIt.instance;
  late final AuthBloc _authBloc =
      AuthBloc(authRepository: getIt<AuthRepository>())
        ..add(AuthLoginStarted());

  Route onGenerateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      //splash screen
      case SplashScreen.route:
        return MaterialPageRoute(
          builder: (_) {
            return const Directionality(
              textDirection: TextDirection.rtl,
              child: SplashScreen(),
            );
          },
        );

      //login screen
      case LoginScreen.route:
        return MaterialPageRoute(
          builder: (_) {
            return BlocProvider.value(
              value: _authBloc,
              child: const Directionality(
                textDirection: TextDirection.rtl,
                child: LoginScreen(),
              ),
            );
          },
        );

      //register screen
      case RegisterScreen.route:
        return MaterialPageRoute(
          builder: (_) {
            return BlocProvider.value(
              value: _authBloc,
              child: const Directionality(
                textDirection: TextDirection.rtl,
                child: RegisterScreen(),
              ),
            );
          },
        );

      //forget password screen
      case ForgetPasswordScreen.route:
        return MaterialPageRoute(
          builder: (_) {
            return BlocProvider.value(
              value: _authBloc,
              child: const Directionality(
                textDirection: TextDirection.rtl,
                child: ForgetPasswordScreen(),
              ),
            );
          },
        );

      //otp screen after register screen
      case OtpScreen.routeAfterRegister:
        return MaterialPageRoute(
          builder: (_) {
            return BlocProvider.value(
              value: _authBloc,
              child: const Directionality(
                textDirection: TextDirection.rtl,
                child: OtpScreen(
                  title: AuthOtp.title,
                  desc: AuthOtp.desc,
                ),
              ),
            );
          },
        );

      //otp screen after forgetPassword screen
      case OtpScreen.routeAfterForgetPassword:
        return MaterialPageRoute(
          builder: (_) {
            return BlocProvider.value(
              value: _authBloc,
              child: const Directionality(
                textDirection: TextDirection.rtl,
                child: OtpScreen(
                  title: AuthForgetPasswordOtp.title,
                  desc: AuthForgetPasswordOtp.desc,
                ),
              ),
            );
          },
        );

      //home screen
      case HomeScreen.route:
        return MaterialPageRoute(builder: (context) {
          return const Directionality(
            textDirection: TextDirection.rtl,
            child: HomeScreen(),
          );
        });

      case TodoScreen.route:
        return MaterialPageRoute(builder: (context) {
          return const Directionality(
            textDirection: TextDirection.rtl,
            child: TodoScreen(),
          );
        });

      case NotesScreen.route:
        return MaterialPageRoute(builder: (context) {
          return const Directionality(
            textDirection: TextDirection.rtl,
            child: NotesScreen(),
          );
        });

      case EditNoteScreen.route:
        return MaterialPageRoute(builder: (context) {
          return const Directionality(
            textDirection: TextDirection.rtl,
            child: EditNoteScreen(),
          );
        });

      case SettingScreen.route:
        return MaterialPageRoute(builder: (context) {
          return const Directionality(
            textDirection: TextDirection.rtl,
            child: SettingScreen(),
          );
        });

      //changePassword screen
      case ChangePasswordScreen.route:
        return MaterialPageRoute(builder: (context) {
          return const Directionality(
            textDirection: TextDirection.rtl,
            child: ChangePasswordScreen(),
          );
        });

      case AppCons.route:
        return MaterialPageRoute(builder: (context) {
          return Directionality(
            textDirection: TextDirection.rtl,
            child: AppCons(),
          );
        });

      //newTicket Screen
      case AddNewTicketScreen.route:
        return MaterialPageRoute(
          builder: (context) {
            return const Directionality(
              textDirection: TextDirection.rtl,
              child: AddNewTicketScreen(),
            );
          },
        );

      case AnswerTicketScreen.route:
        return MaterialPageRoute(
          builder: (context) {
            //RouteSettings settings = ModalRoute.of(context).settings;
            return const Directionality(
              textDirection: TextDirection.rtl,
              child: AnswerTicketScreen(),
            );
          },
        );

      case ReplyTicketScreen.route:
        return MaterialPageRoute(
          builder: (context) {
            //RouteSettings settings = ModalRoute.of(context).settings;
            return const Directionality(
              textDirection: TextDirection.rtl,
              child: ReplyTicketScreen(),
            );
          },
        );

      //listTicket screen

      case Clander.route:
        return MaterialPageRoute(
          builder: (context) {
            //RouteSettings settings = ModalRoute.of(context).settings;
            return Directionality(
              textDirection: TextDirection.rtl,
              child: Clander(),
            );
          },
        );

      case ListTicketScreen.route:
        return MaterialPageRoute(
          builder: (context) {
            return const Directionality(
              textDirection: TextDirection.rtl,
              child: ListTicketScreen(),
            );
          },
        );
      default:
        throw Exception('root is not valid');
    }
  }

  void dispose() {
    _authBloc.close();
  }
}
